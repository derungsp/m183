export class CredentialsCheckResponse {
    userId: number;
    phone: string;
}