import { CommonModule } from '@angular/common';
import { Component, NgModule, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/Post';
import { User } from 'src/app/models/User';
import { Globals } from 'src/app/providers/globals';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html'
})
// @NgModule({
//   imports: [CommonModule],
//   declarations: [AdminDashboardComponent]
// })
export class AdminDashboardComponent implements OnInit {
  users: User[];
  posts: Post[];

  constructor(public globals: Globals, private apiService: ApiService, private router: Router) { }

  ngOnInit(): void {
    // this.users = this.apiService.getAllUsers();
    if (!this.globals.loggedIn) {
      this.router.navigate(['/']);
    }
    else if (this.globals.user.role.name != "Admin") {
      this.router.navigate(['/']);
    }
    else {
      // this.apiService.getAllUsers().subscribe(res => this.users = res);
      this.apiService.getAllPosts().subscribe(res => this.posts = res);
    }
  }

  // selectUser(user){
  //   this.globals.selectedUser = user;
  //   this.router.navigate(['/admin/user/edit']);
  // }

  addPost() {
    this.globals.selectedPost = new Post();
    this.router.navigate(['/post/edit']);
  }

  deletePost(post: Post) {
    this.posts = this.apiService.deletePost(post);
    // this.router.navigate(['/admin/dashboard']);
  }

  revertDeletionPost(post: Post) {
    this.posts = this.apiService.revertDeletePost(post);
    // this.router.navigate(['/admin/dashboard']);
  }

  selectPost(post) {
    this.globals.selectedPost = post;
    this.router.navigate(['/post/edit']);
  }

  // addUser(){
  //   this.globals.selectedUser = new User();
  //   this.router.navigate(['/admin/user/edit']);
  // }
}
