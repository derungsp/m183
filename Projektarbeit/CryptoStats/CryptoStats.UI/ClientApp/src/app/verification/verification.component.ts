import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Verification } from '../models/Verification';
import { ApiService } from '../services/api.service';
import { Globals } from '../providers/globals';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html'
})
export class VerificationComponent implements OnInit {
  verificationForm: FormGroup;
  submitted = false;
  verificationModel = new Verification();
  userId;


  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.globals.user) {
      this.router.navigate(['/']);
    }
    else{
      if (!this.globals.user.id) {
        this.router.navigate(['/']);
      }
    }
    this.globals.error = null;
    this.globals.user = JSON.parse(localStorage.getItem("user"));
    this.verificationForm = this.formBuilder.group({
      pin: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.verificationForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.verificationForm.invalid) {
      return;
    }

    if (this.globals.user) {
      this.verificationModel.userId = this.globals.user.id;
      this.verificationModel.value = this.verificationForm.value.pin;
    }

    // Process checkout data here
    this.verificationForm.reset();

    this.apiService.verificate(this.verificationModel);
  }
}