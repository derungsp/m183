﻿using CryptoStats.DataAccess.Repository;
using CryptoStats.DataAccess.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;

namespace CryptoStats.DataAccess.Interfaces
{
    public interface IPostRepository : IRepository<Post>
    {
        Task<IEnumerable<Post>> GetAllMatchingPostsWithComments(Expression<Func<Post, bool>> predicate);
    }
}
