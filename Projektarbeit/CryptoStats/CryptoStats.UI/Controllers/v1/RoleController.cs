﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using CryptoStats.Businesslayer.Roles.Interfaces;
using CryptoStats.Businesslayer.Roles.Models;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CryptoStats.UI.Controllers.v1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet]
        public async Task<IEnumerable<GetRoleDTO>> GetAll()
        {
            return await _roleService.GetAll();
        }
    }
}
