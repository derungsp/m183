﻿namespace CryptoStats.Businesslayer.Users.Models
{
    public class AddUserDTO
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Username { get; set; }

        public string Secret { get; set; }

        public string Password { get; set; }
        
        public bool Active { get; set; }

        public int RoleId { get; set; }

    }
}
