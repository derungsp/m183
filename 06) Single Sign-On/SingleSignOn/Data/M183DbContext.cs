using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SingleSignOn.Models;

namespace SingleSignOn.Data
{
    public class M183DbContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public DbSet<User> Users { get; set; }

        public M183DbContext(DbContextOptions<M183DbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User { Id = Guid.NewGuid(), FirstName = "pascal", LastName = "derungs", Email = "pascal6313@gmail.com"}
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}