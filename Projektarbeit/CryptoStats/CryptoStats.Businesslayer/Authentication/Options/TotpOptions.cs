﻿namespace CryptoStats.Businesslayer.Authentication.Options
{
    public class TotpOptions
    {
        public string ManualEntryKey { get; set; }

        public string QrCodeSetupImageUrl { get; set; }

        public string Username { get; set; }

    }
}
