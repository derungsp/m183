﻿using CryptoStats.Businesslayer.Posts.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoStats.Businesslayer.Posts.Interfaces
{
    public interface IPostService
    {
        Task<IEnumerable<GetPostDTO>> GetAllPublicPosts();

        Task<IEnumerable<GetPostDTO>> GetAllPosts();

        Task Create(AddPostDTO post);

        Task<IEnumerable<GetPostDTO>> GetOwnPosts();

        Task<IEnumerable<GetPostDTO>> Update(UpdatePostDTO post);

        Task<IEnumerable<GetPostDTO>> Delete(GetPostDTO post);

        Task<IEnumerable<GetPostDTO>> RevertDeletion(GetPostDTO post);

    }
}
