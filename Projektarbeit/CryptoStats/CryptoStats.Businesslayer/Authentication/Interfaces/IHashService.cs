﻿namespace CryptoStats.Businesslayer.Authentication.Interfaces
{
    public interface IHashService
    {
        string CreateHash(string value, string salt);

        bool Validate(string value, string salt, string hash);

        string CreateSalt();

    }
}
