﻿using CryptoStats.DataAccess.Interfaces;
using CryptoStats.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CryptoStats.DataAccess.Repository
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        public PostRepository(CryptoStatsContext dbContext) : base(dbContext)
        {
        }

        public async Task<IEnumerable<Post>> GetAllMatchingPostsWithComments(Expression<Func<Post, bool>> predicate)
        {
            return await _dbContext.Set<Post>().Where(predicate).Include(x => x.Comments).ToListAsync();
        }
    }
}
