﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using CryptoStats.Businesslayer.Users.Interfaces;
using CryptoStats.Businesslayer.Users.Models;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using CryptoStats.Businesslayer.Authentication.Models;
using CryptoStats.Businesslayer.Authentication.Interfaces;
using System;
using CryptoStats.Businesslayer.Authentication.Options;
using Microsoft.Extensions.Options;

namespace CryptoStats.UI.Controllers.v1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;
        private readonly IJwtService _jwtService;
        private readonly JwtOptions _jwtOptions;

        public UserController(IUserService userService, ILogger<UserController> logger, IJwtService jwtService, IOptions<JwtOptions> jwtOptions)
        {
            _userService = userService;
            _logger = logger;
            _jwtService = jwtService;
            _jwtOptions = jwtOptions.Value;
        }

        /// <summary>
        /// Gets every user
        /// </summary>
        /// <returns>List of UserDTO</returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IEnumerable<GetUserDTO>> GetAll()
        {
            return await _userService.GetAll();
        }

        /// <summary>
        /// Gets a user by id
        /// </summary>
        /// <param name="id">id of user</param>
        /// <returns>UserDTO</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            GetUserDTO user = await _userService.GetById(id);

            if (user != null)
            {
                return Ok(user);
            }

            return Unauthorized();
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="user">UserDTO</param>
        [HttpPost]
        [AllowAnonymous]
        public async Task<GetUserDTO> Create([FromBody] AddUserDTO user)
        {
            if (!await _userService.EmailExists(user.Email) && !await _userService.UsernameExists(user.Username) && !await _userService.PhoneExists(user.Phone))
            {
                await _userService.Create(user);

                GetUserDTO userResponse = await _userService.GetByUsername(user.Username);

                _logger.LogInformation("User [" + user.Firstname + " | " + user.Lastname + "] was successfully created");

                string token = _jwtService.CreateToken(userResponse);

                userResponse.JwtResponse = new JwtResponseDTO { Token = token, ExpiryDate = DateTime.Now.AddDays(_jwtOptions.TokenExpiryInDays) };

                return userResponse;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Updates single or multiple user
        /// </summary>
        /// <param name="users">List of UserDTO</param>
        [HttpPut]
        public async Task<GetUserDTO> Update([FromBody] UpdateUserDTO user)
        {
            await _userService.Update(user);

            _logger.LogInformation("User [" + user.Firstname + " | " + user.Lastname + "] was successfully updated");

            GetUserDTO userResponse = await _userService.GetByUsername(user.Username);

            string token = _jwtService.CreateToken(userResponse);

            userResponse.JwtResponse = new JwtResponseDTO { Token = token, ExpiryDate = DateTime.Now.AddDays(_jwtOptions.TokenExpiryInDays) };

            return userResponse;
        }

        /// <summary>
        /// Deletes user by id
        /// </summary>
        /// <param name="id">id of user</param>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async void DeleteById(int id)
        {
            await _userService.Delete(id);
        }
    }
}
