﻿using CryptoStats.Businesslayer.Users.Models;

namespace CryptoStats.Businesslayer.Authentication.Interfaces
{
    public interface IJwtService
    {
        string CreateToken(GetUserDTO user);
    }
}
