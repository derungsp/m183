﻿using CryptoStats.Businesslayer.Roles.Interfaces;
using CryptoStats.Businesslayer.Roles.Models;
using CryptoStats.DataAccess.Repository;
using CryptoStats.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace CryptoStats.Businesslayer.Roles
{
    public class RoleService : IRoleService
    {
        private readonly IRepository<Role> _roleRepository;
        private readonly IMapper _mapper;

        public RoleService(IRepository<Role> roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<GetRoleDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<GetRoleDTO>>(await  _roleRepository.GetAllAsync());
        }

    }
}
