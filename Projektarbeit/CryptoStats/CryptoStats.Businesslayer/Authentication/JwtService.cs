﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Authentication.Options;
using CryptoStats.Businesslayer.Users.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Security.Claims;
using System;

namespace CryptoStats.Businesslayer.Authentication
{
    public class JwtService : IJwtService
    {
        private readonly JwtOptions _jwtOptions;

        public JwtService(IOptions<JwtOptions> jwtOptions)
        {
            _jwtOptions = jwtOptions.Value;
        }

        /// <summary>
        /// Creates a token for a user including a list of claims
        /// </summary>
        /// <param name="user">Logged in user as a User object</param>
        /// <returns>JWT as a string</returns>
        public string CreateToken(GetUserDTO user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var claims = CreateClaimsIdentities(user);

            var token = tokenHandler.CreateJwtSecurityToken(
                subject: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddDays(_jwtOptions.TokenExpiryInDays),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_jwtOptions.PrivateKeyAsBytes), SecurityAlgorithms.HmacSha256Signature));

            return tokenHandler.WriteToken(token);
        }

        /// <summary>
        /// Creates a new list of Claims for the logged in user
        /// </summary>
        /// <param name="user">Logged in user as a User object</param>
        /// <returns>List of Claims</returns>
        public ClaimsIdentity CreateClaimsIdentities(GetUserDTO user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.GivenName, user.Firstname),
                new Claim(ClaimTypes.Name, user.Lastname),
                new Claim(ClaimTypes.Role, user.Role.Name)
            };

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims);

            return claimsIdentity;
        }
    }
}
