﻿namespace CryptoStats.Businesslayer.Authentication.Models
{
    public class CredentialsCheckResponseDTO
    {
        public int UserId { get; set; }
        public string Phone { get; set; }
    }
}
