﻿using CryptoStats.Businesslayer.Posts.Models;
using FluentValidation;

namespace CryptoStats.Businesslayer.Posts.Validators
{
    public class GetPostValidator : AbstractValidator<GetPostDTO>
    {
        public GetPostValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.Title).NotNull();
            RuleFor(x => x.Content).NotNull();
            RuleFor(x => x.Public).NotNull();
            RuleFor(x => x.Deleted).NotNull();
            RuleFor(x => x.Timestamp).NotNull();
            RuleFor(x => x.UserId).NotNull();
        }
    }
}
