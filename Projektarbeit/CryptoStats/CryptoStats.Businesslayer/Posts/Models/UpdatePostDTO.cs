﻿namespace CryptoStats.Businesslayer.Posts.Models
{
    public class UpdatePostDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public bool Public { get; set; }

        public bool Deleted { get; set; }

        public int UserId { get; set; }

    }
}
