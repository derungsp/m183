﻿using System.Collections.Generic;
using System.Security.Claims;

namespace CryptoStats.Businesslayer.Authentication.Interfaces
{
    public interface IUserIdentityService
    {
        IEnumerable<Claim> GetClaims();

        int GetId();

    }
}
