import { Injectable } from '@angular/core';
import { Post } from '../models/Post';
import { User } from '../models/User';

@Injectable()
export class Globals {
  user: User;
  selectedPost: Post;
  selectedUser: User;
  tempUser: User;
  tempPost: Post;
  error: string;
  loggedIn: boolean = false;
  posts: Post[];
}