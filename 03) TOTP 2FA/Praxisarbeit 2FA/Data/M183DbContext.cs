﻿using Microsoft.EntityFrameworkCore;
using Praxisarbeit_2FA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Praxisarbeit_2FA.Data
{
    public class M183DbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public M183DbContext(DbContextOptions<M183DbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User { Id = Guid.NewGuid(), Username = "donald", Password = "niceHair" },
                new User { Id = Guid.NewGuid(), Username = "joe", Password = "kamala" }
            );

            //Database.EnsureCreated();

            base.OnModelCreating(modelBuilder);
        }
    }
}
