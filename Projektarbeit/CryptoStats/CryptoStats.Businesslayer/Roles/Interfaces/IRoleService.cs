﻿using CryptoStats.Businesslayer.Roles.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoStats.Businesslayer.Roles.Interfaces
{
    public interface IRoleService
    {
        Task<IEnumerable<GetRoleDTO>> GetAll();

    }
}
