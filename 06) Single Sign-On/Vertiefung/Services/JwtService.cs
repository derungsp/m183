﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Authentication.Options;
using CryptoStats.Businesslayer.Authentication.Models;
using CryptoStats.Businesslayer.Users.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Security.Claims;
using System;

namespace CryptoStats.Businesslayer.Authentication
{
    public class JwtService : IJwtService
    {
        private readonly JwtOptions _jwtOptions;

        public JwtService(IOptions<JwtOptions> jwtOptions)
        {
            _jwtOptions = jwtOptions.Value;
        }

        public JwtResponseDTO CreateToken(UserDTO user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var claims = CreateClaimsIdentities(user);

            var token = tokenHandler.CreateJwtSecurityToken(
                subject: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddDays(_jwtOptions.TokenExpiryInDays),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_jwtOptions.PrivateKeyAsBytes), SecurityAlgorithms.HmacSha256Signature)
            );

            return new JwtResponseDTO { Token = tokenHandler.WriteToken(token), ExpiryDate = DateTime.UtcNow.AddDays(_jwtOptions.TokenExpiryInDays) };
        }

        public ClaimsIdentity CreateClaimsIdentities(UserDTO user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.GivenName, user.Firstname),
                new Claim(ClaimTypes.Name, user.Lastname)
            };

            return new ClaimsIdentity(claims);
        }
    }
}
