﻿using CryptoStats.Businesslayer.Verifications.Interfaces;
using CryptoStats.Businesslayer.Verifications.Models;
using CryptoStats.DataAccess.Repository;
using CryptoStats.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using System;

namespace CryptoStats.Businesslayer.Verifications
{
    public class VerificationService : IVerificationService
    {
        private readonly IRepository<Verification> _verificationRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IMapper _mapper;

        public VerificationService(IRepository<Verification> verificationRepository, IMapper mapper, IRepository<User> userRepository)
        {
            _verificationRepository = verificationRepository;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<GetVerificationDTO> GetById(int id)
        {
            return _mapper.Map<GetVerificationDTO>(await _verificationRepository.GetFirstMatching(u => u.Id == id));
        }

        public async Task<GetVerificationDTO> GetByUserId(int userId)
        {
            return _mapper.Map<GetVerificationDTO>(await _verificationRepository.GetFirstMatching(u => u.UserId == userId));
        }

        public async Task<bool> Check(GetVerificationDTO verificationDTO)
        {
            IEnumerable<Verification> verifications = await _verificationRepository.GetAllMatchingAsync(x => x.UserId == verificationDTO.UserId);

            Verification verification = verifications.ToList().Last();

            if (verification != null)
            {
                if (verification.Value == verificationDTO.Value && verification.Timestamp > DateTime.Now.AddMinutes(-5))
                {
                    return true;
                }

                return false;
            }
            return false;
        }

        public async Task<bool> CheckSecret(SecretDTO secret)
        {
            User user = await _userRepository.GetByIdAsync(secret.UserId);

            if (user != null && secret.Value == user.Secret)
            {
                return true;
            }

            return false;
        }


        public async Task Create(AddVerificationDTO verificationDTO)
        {
            Verification verification = new Verification
            {
                UserId = verificationDTO.UserId,
                Value = verificationDTO.Value,
                Timestamp = DateTime.Now
            };

            await _verificationRepository.InsertAsync(verification);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task Delete(int userId)
        {
            IEnumerable<Verification> verifications = await _verificationRepository.GetAllMatchingAsync(x => x.UserId == userId);

            foreach (Verification verification in verifications)
            {
                await _verificationRepository.DeleteAsync(verification);
            }
        }
    }
}
