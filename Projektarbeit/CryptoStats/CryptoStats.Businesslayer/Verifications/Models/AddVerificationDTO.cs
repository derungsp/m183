﻿namespace CryptoStats.Businesslayer.Verifications.Models
{
    public class AddVerificationDTO
    {
        public int Id { get; set; }

        public int Value { get; set; }

        public int UserId { get; set; }

    }
}
