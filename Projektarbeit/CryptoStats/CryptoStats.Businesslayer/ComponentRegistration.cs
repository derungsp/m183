﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Verifications.Interfaces;
using CryptoStats.Businesslayer.Verifications.Models;
using CryptoStats.Businesslayer.Comments.Interfaces;
using CryptoStats.Businesslayer.Roles.Validators;
using CryptoStats.Businesslayer.Users.Validators;
using CryptoStats.Businesslayer.Users.Interfaces;
using CryptoStats.Businesslayer.Posts.Interfaces;
using CryptoStats.Businesslayer.Roles.Interfaces;
using CryptoStats.Businesslayer.Comments.Models;
using Microsoft.Extensions.DependencyInjection;
using CryptoStats.Businesslayer.Authentication;
using CryptoStats.Businesslayer.Verifications;
using CryptoStats.Businesslayer.Roles.Models;
using CryptoStats.Businesslayer.Users.Models;
using CryptoStats.Businesslayer.Posts.Models;
using CryptoStats.Businesslayer.Comments;
using CryptoStats.DataAccess.Interfaces;
using CryptoStats.DataAccess.Repository;
using CryptoStats.Businesslayer.Posts;
using CryptoStats.Businesslayer.Roles;
using CryptoStats.Businesslayer.Users;
using CryptoStats.DataAccess.Models;
using FluentValidation;
using AutoMapper;
using CryptoStats.Businesslayer.Posts.Validators;

namespace CryptoStats.Businesslayer
{
    public class ComponentRegistration
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            //AutoMapper
            services.AddAutoMapper(typeof(ComponentRegistration));

            // Authentication
            services.AddTransient<IHashService, HashService>();
            services.AddTransient<IUserIdentityService, UserIdentityService>();
            services.AddTransient<IJwtService, JwtService>();

            // Comments
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IRepository<Comment>, Repository<Comment>>();

            // Posts
            services.AddTransient<IValidator<GetPostDTO>, GetPostValidator>();
            services.AddTransient<IValidator<AddPostDTO>, AddPostValidator>();
            services.AddTransient<IValidator<UpdatePostDTO>, UpdatePostValidator>();
            services.AddTransient<IPostService, PostService>();
            services.AddTransient<IRepository<Post>, Repository<Post>>();
            services.AddTransient<IPostRepository, PostRepository>();

            // Roles
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IValidator<RoleDTO>, RoleValidator>();
            services.AddTransient<IRepository<Role>, Repository<Role>>();

            // Users
            services.AddTransient<IValidator<GetUserDTO>, GetUserValidator>();
            services.AddTransient<IValidator<AddUserDTO>, AddUserValidator>();
            services.AddTransient<IValidator<UpdateUserDTO>, UpdateUserValidator>();
            services.AddTransient<IRepository<User>, Repository<User>>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserService, UserService>();

            // Verifications
            services.AddTransient<IRepository<Verification>, Repository<Verification>>();
            services.AddTransient<IVerificationService, VerificationService>();

        }
    }

    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            // Comment
            CreateMap<Comment, GetCommentDTO>();
            CreateMap<GetCommentDTO, Comment>();
            CreateMap<Comment, AddCommentDTO>();
            CreateMap<AddCommentDTO, Comment>();

            // Post
            CreateMap<Post, GetPostDTO>();
            CreateMap<GetPostDTO, Post>();
            CreateMap<Post, AddPostDTO>();
            CreateMap<AddPostDTO, Post>();
            CreateMap<Post, UpdatePostDTO>();
            CreateMap<UpdatePostDTO, Post>();

            // User
            CreateMap<User, GetUserDTO>();
            CreateMap<GetUserDTO, User>();
            CreateMap<User, AddUserDTO>();
            CreateMap<AddUserDTO, User>();
            CreateMap<Verification, GetVerificationDTO>();
            CreateMap<GetVerificationDTO, Verification>();
            CreateMap<Verification, AddVerificationDTO>();
            CreateMap<AddVerificationDTO, Verification>();
            CreateMap<User, UpdateUserDTO>();
            CreateMap<UpdateUserDTO, User>();

            // Role
            CreateMap<Role, GetRoleDTO>();
            CreateMap<GetRoleDTO, Role>();

        }
    }
}
