﻿using CryptoStats.Businesslayer.Users.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoStats.Businesslayer.Users.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<GetUserDTO>> GetAll();

        Task<GetUserDTO> GetById(int id);

        Task<GetUserDTO> GetByBasicAuthentication(string username, string password);

        Task<GetUserDTO> GetByEmail(string email);

        Task<GetUserDTO> GetByUsername(string username);

        Task<string> GetUsername(int id);

        Task<bool> UsernameExists(string username);

        Task<bool> EmailExists(string email);

        Task<bool> PhoneExists(string phone);

        Task Create(AddUserDTO user);

        Task Update(UpdateUserDTO user);

        Task Delete(int id);

    }
}
