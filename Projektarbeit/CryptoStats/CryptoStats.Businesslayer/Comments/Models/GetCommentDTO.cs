﻿namespace CryptoStats.Businesslayer.Comments.Models
{
    public class GetCommentDTO
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public string Creator { get; set; }

        public int PostId { get; set; }

        public int UserId { get; set; }

    }
}
