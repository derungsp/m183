import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { Verification } from 'src/app/models/Verification';
import { Globals } from 'src/app/providers/globals';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-user-edit-verification',
  templateUrl: './user-edit-verification.component.html'
})
export class UserEditVerificationComponent implements OnInit {
  phoneVerificationForm: FormGroup;
  submitted = false;
  userId;
  user: User;
  verificationModel = new Verification();


  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.globals.user) {
      this.router.navigate(['/']);
    }
    else{
      if (!this.globals.user.id) {
        this.router.navigate(['/']);
      }
    }
    this.globals.error = null;
    this.globals.user = JSON.parse(localStorage.getItem("user"));
    this.phoneVerificationForm = this.formBuilder.group({
      pin: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.phoneVerificationForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.phoneVerificationForm.invalid) {
      return;
    }

    if (this.globals.user) {
      this.verificationModel.userId = this.globals.user.id;
      this.verificationModel.value = this.phoneVerificationForm.value.pin;
    }

    // Process checkout data here
    this.phoneVerificationForm.reset();

    this.apiService.verificatePhoneChangeVerification(this.verificationModel);
  }
}