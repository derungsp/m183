﻿using CryptoStats.Businesslayer.Comments.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoStats.Businesslayer.Comments.Interfaces
{
    public interface ICommentService
    {
        Task<IEnumerable<GetCommentDTO>> GetAllCommentsToPost(int postId);

        Task Create(AddCommentDTO comment);

    }
}
