import { Component, OnInit } from '@angular/core';
import { Globals } from './providers/globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(public globals: Globals) { 
    if (localStorage.getItem("user")) {
      this.globals.user = JSON.parse(localStorage.getItem("user"));
      if (this.globals.user.jwtResponse) {
        this.globals.loggedIn = true;
      }
    }
  }
  title = 'app';
}