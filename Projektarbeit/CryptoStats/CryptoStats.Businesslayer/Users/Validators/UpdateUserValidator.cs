﻿using CryptoStats.Businesslayer.Users.Models;
using FluentValidation;

namespace CryptoStats.Businesslayer.Users.Validators
{
    public class UpdateUserValidator : AbstractValidator<UpdateUserDTO>
    {
        public UpdateUserValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.Phone).Matches("^(\\b(0041|0)|\\B\\+41)(\\s?\\(0\\))?(\\s)?[1-9]{2}(\\s)?[0-9]{3}(\\s)?[0-9]{2}(\\s)?[0-9]{2}\\b");
        }
    }
}
