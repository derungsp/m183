﻿using System.Text;

namespace CryptoStats.Businesslayer.Authentication.Options
{
    public class JwtOptions
    {
        public string PrivateKey { get; set; }

        public int TokenExpiryInDays { get; set; }

        public byte[] PrivateKeyAsBytes
        {
            get
            {
                return Encoding.UTF8.GetBytes(PrivateKey);
            }
        }
    }
}
