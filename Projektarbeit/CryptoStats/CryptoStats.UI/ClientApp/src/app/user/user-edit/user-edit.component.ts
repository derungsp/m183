import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';
import { Globals } from 'src/app/providers/globals';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit {
  user: User;
  userForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.globals.user || !this.globals.loggedIn) {
      this.router.navigate(['/']);
    }
    else {
      this.user = this.globals.user;
    }

    this.userForm = this.formBuilder.group({
      phone: ['', Validators.required]
    });

    this.globals.tempUser = null;
  }

  // convenience getter for easy access to form fields
  get f() { return this.userForm.controls; }

  logout() {
    this.apiService.logout();
  }

  onSubmit() {
    this.globals.error = null;
    this.submitted = true;

    // stop here if form is invalid
    if (this.userForm.invalid) {
      return;
    }

    this.user.phone = this.userForm.value.phone;
    this.user.roleId = this.globals.user.role.id;

    this.globals.tempUser = this.user;

    this.apiService.phoneChangeVerification(this.user);
  }
}