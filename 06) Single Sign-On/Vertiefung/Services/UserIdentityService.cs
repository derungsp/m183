﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Linq;
using System;

namespace CryptoStats.Businesslayer.Authentication
{
    public class UserIdentityService : IUserIdentityService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserIdentityService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<Claim> GetClaims()
        {
            if (_httpContextAccessor?.HttpContext?.User?.Identity is ClaimsIdentity identity)
            {
                return identity.Claims;
            }

            return new List<Claim>();
        }

        public int GetId()
        {
            if (_httpContextAccessor?.HttpContext?.User?.Identity is ClaimsIdentity identity)
            {
                return Convert.ToInt32(identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value);
            }

            return default;
        }
    }
}
