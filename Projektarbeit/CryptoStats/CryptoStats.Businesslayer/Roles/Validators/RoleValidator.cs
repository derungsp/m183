﻿using CryptoStats.Businesslayer.Roles.Models;
using FluentValidation;

namespace CryptoStats.Businesslayer.Roles.Validators
{
    public class RoleValidator : AbstractValidator<RoleDTO>
    {
        public RoleValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
