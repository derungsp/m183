import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from '../providers/globals';

@Injectable({
  providedIn: 'root'
})
export class SmsService {
  gatewayURL: string = 'https://m183.gibz-informatik.ch/api/sms/message';

  constructor(private httpClient: HttpClient, private router: Router, private globals: Globals) { }

  pin = this.generatePin();

  public send(phone: string): number {
    if (phone.startsWith("+")) {
      phone = phone.substring(1);
    }
    else{
      phone = phone.substring(1);
      phone = "41" + phone;
    }

    const payload = {
      "mobileNumber": phone,
      "message": "Use the code " + this.pin + " to authenticate with our great app!"
    };
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-Api-Key': 'NQA3ADEANwA2ADcAOQAyADgANAAzADYANwAzADcANwA1ADUA'
    });

    this.httpClient.post<any>(`${this.gatewayURL}`, payload, { observe: 'response', headers: headers }).subscribe(response => {
      if (response.status == 204) {
        if (this.globals.tempUser) {
          this.router.navigate(['/user/edit/verification']);
        }
        else if (this.globals.tempPost) {
          this.router.navigate(['/post/edit/verification']);
        }
        else{
          this.router.navigate(['/verification']);
        }
      }
    });
    return this.pin;
  }

  private generatePin(): number {
    return Math.floor(100000 + Math.random() * 900000);
  }
}