import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Globals } from '../providers/globals';
import { ApiService } from '../services/api.service';
import { Comment } from '../models/Comment';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html'
})
export class PostComponent implements OnInit {
  commentForm: FormGroup;
  submitted = false;
  comment: Comment;

  constructor(public globals: Globals, private router: Router, private formBuilder: FormBuilder, private apiService: ApiService) { }

  ngOnInit(): void {
    this.commentForm = this.formBuilder.group({
      content: ['', [Validators.required, Validators.maxLength(200)]]
    });

    this.comment = new Comment();

    if (!this.globals.selectedPost) {
      this.router.navigate(['/']);
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.commentForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.commentForm.invalid) {
      return;
    }
    
    this.comment.postId = this.globals.selectedPost.id;
    this.comment.content = this.commentForm.value.content;
    this.comment.userId = this.globals.user.id; 
    
    // Process checkout data here
    this.commentForm.reset();

    this.apiService.addComment(this.comment);
  }
}
