﻿using CryptoStats.Businesslayer.Comments.Interfaces;
using CryptoStats.Businesslayer.Comments.Models;
using CryptoStats.DataAccess.Repository;
using CryptoStats.DataAccess.Models;
using System.Threading.Tasks;
using AutoMapper;
using System.Collections.Generic;
using CryptoStats.Businesslayer.Users.Interfaces;

namespace CryptoStats.Businesslayer.Comments
{
    public class CommentService : ICommentService
    {
        private readonly IRepository<Comment> _commentRepository;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public CommentService(IRepository<Comment> commentRepository, IMapper mapper, IUserService userService)
        {
            _commentRepository = commentRepository;
            _mapper = mapper;
            _userService = userService;
        }

        public async Task<IEnumerable<GetCommentDTO>> GetAllCommentsToPost(int postId)
        {
            IEnumerable<GetCommentDTO> comments = _mapper.Map<IEnumerable<GetCommentDTO>>(await _commentRepository.GetAllMatchingAsync(x => x.PostId == postId));

            foreach (GetCommentDTO c in comments)
            {
                c.Creator = await _userService.GetUsername(c.UserId);
            }

            return comments;
        }

        public async Task Create(AddCommentDTO comment)
        {
            await _commentRepository.InsertAsync(_mapper.Map<Comment>(comment));
        }
    }
}
