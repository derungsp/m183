import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/User';
import { Globals } from '../providers/globals';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  user = new User();
  secret: string;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit() {
    this.secret = this.makeRandom(20);
    this.globals.error = null;
    if (this.globals.loggedIn) {
      this.router.navigate(['/']);
    }
    this.registerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      phone: ['', [Validators.required, Validators.pattern('^(\\b(0041|0)|\\B\\+41)(\\s?\\(0\\))?(\\s)?[1-9]{2}(\\s)?[0-9]{3}(\\s)?[0-9]{2}(\\s)?[0-9]{2}\\b')]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.registerForm.value.secret = this.secret;
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  makeRandom(lengthOfCode: number): string {
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890[]=)(&%$#@!";
    let text = "";
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }


  onSubmit() {
    this.submitted = true;
    this.globals.error = null;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.user.firstname = this.registerForm.value.firstname;
    this.user.lastname = this.registerForm.value.lastname;
    this.user.username = this.registerForm.value.username;
    this.user.email = this.registerForm.value.email;
    this.user.phone = this.registerForm.value.phone;
    this.user.password = this.registerForm.value.password;
    this.user.roleId = 2;
    this.user.secret = this.secret;
    this.user.active = true;

    // Process checkout data here
    this.registerForm.reset();

    this.apiService.register(this.user);
  }
}