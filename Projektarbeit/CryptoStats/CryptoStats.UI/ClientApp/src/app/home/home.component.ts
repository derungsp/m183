import { Component, OnInit } from '@angular/core';
import { Post } from '../models/Post';
import { ApiService } from '../services/api.service';
import { Globals } from '../providers/globals';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  posts: Post[];

  constructor(
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit() {
    this.apiService.getAllPublicPosts().subscribe(res=>this.posts=res);
  }

  public open(event, item) {
    this.globals.selectedPost = item;
    this.router.navigate(['/post']);
  }
}
