﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Users.Interfaces;
using CryptoStats.Businesslayer.Users.Models;
using CryptoStats.DataAccess.Interfaces;
using CryptoStats.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using System;

namespace CryptoStats.Businesslayer.Users
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IHashService _hashService;
        private readonly IMapper _mapper;
        private readonly IUserIdentityService _userIdentityService;

        public UserService(IUserRepository userRepository, IHashService hashService, IMapper mapper, IUserIdentityService userIdentityService)
        {
            _userRepository = userRepository;
            _hashService = hashService;
            _mapper = mapper;
            _userIdentityService = userIdentityService;
        }

        public async Task<IEnumerable<GetUserDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<GetUserDTO>>(await _userRepository.GetAllUsersWithRole());
        }

        public async Task<GetUserDTO> GetById(int userId)
        {
            return _mapper.Map<GetUserDTO>(await _userRepository.GetFirstMatchingUserWithRole(x => x.Id == userId));
        }

        public async Task<bool> UsernameExists(string username)
        {
            GetUserDTO user = _mapper.Map<GetUserDTO>(await _userRepository.GetFirstMatching(u => u.Username == username));

            if (user != null)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> EmailExists(string email)
        {
            GetUserDTO user = _mapper.Map<GetUserDTO>(await _userRepository.GetFirstMatching(u => u.Email == email));

            if (user != null)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> PhoneExists(string phone)
        {
            GetUserDTO user = _mapper.Map<GetUserDTO>(await _userRepository.GetFirstMatching(u => u.Phone == phone));

            if (user != null)
            {
                return true;
            }

            return false;
        }

        public async Task<GetUserDTO> GetByBasicAuthentication(string username, string password)
        {
            User user = await _userRepository.GetFirstMatchingUserWithRole(u => u.Username == username);

            if (user != null)
            {
                if (user.PasswordHash == null && !user.Blocked)
                {
                    return _mapper.Map<GetUserDTO>(user);
                }
                else if (_hashService.Validate(password, user.PasswordSalt, user.PasswordHash) && !user.Blocked)
                {
                    return _mapper.Map<GetUserDTO>(user);
                }
                else
                {
                    if (user.Blocked)
                    {
                        if (user.BlockDate > DateTime.Now.AddMinutes(-5))
                        {
                            return new GetUserDTO() { Blocked = true, BlockDate = user.BlockDate, Id = -1 };
                        }
                        else
                        {
                            user.Blocked = false;
                            user.InvalidLoginCount = 0;

                            user.BlockDate = null;

                            await _userRepository.UpdateAsync(user);
                        }
                    }
                    else
                    {
                        user.InvalidLoginCount += 1;

                        if (user.InvalidLoginCount == 3)
                        {
                            user.InvalidLoginCount = 0;
                            user.Blocked = true;
                            user.BlockDate = DateTime.Now;

                            await _userRepository.UpdateAsync(user);

                            return new GetUserDTO() { Blocked = false, Id = -1 };
                        }

                        await _userRepository.UpdateAsync(user);

                        return null;
                    }
                }
            }

            return null;
        }

        public async Task<GetUserDTO> GetByEmail(string email)
        {
            //return _mapper.Map<GetUserDTO>(await _userRepository.GetUser(email));
            return _mapper.Map<GetUserDTO>(await _userRepository.GetFirstMatchingUserWithRole(u => u.Email == email));
        }

        public async Task<GetUserDTO> GetByUsername(string username)
        {
            return _mapper.Map<GetUserDTO>(await _userRepository.GetFirstMatchingUserWithRole(u => u.Username == username));
        }

        public async Task<string> GetUsername(int id)
        {
            return _mapper.Map<GetUserDTO>(await _userRepository.GetByIdAsync(id)).Username;
        }

        public async Task Create(AddUserDTO userDTO)
        {
            var salt = _hashService.CreateSalt();

            User user = new User
            {
                Firstname = userDTO.Firstname,
                Lastname = userDTO.Lastname,
                Email = userDTO.Email,
                Phone = userDTO.Phone,
                PasswordSalt = salt,
                PasswordHash = _hashService.CreateHash(userDTO.Password, salt),
                RoleId = userDTO.RoleId,
                Username = userDTO.Username,
                Active = userDTO.Active,
                Secret = userDTO.Secret,
                Timestamp = DateTime.Now
            };

            await _userRepository.InsertAsync(user);
        }

        public async Task Update(UpdateUserDTO userDTO)
        {
            await _userRepository.UpdateAsync(_mapper.Map<User>(userDTO));
        }

        public async Task Delete(int id)
        {
            await _userRepository.DeleteAsync(await _userRepository.GetFirstMatching(x => x.Id == id));
        }
    }
}