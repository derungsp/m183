﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Verifications.Interfaces;
using CryptoStats.Businesslayer.Authentication.Options;
using CryptoStats.Businesslayer.Authentication.Models;
using CryptoStats.Businesslayer.Verifications.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using CryptoStats.Businesslayer.Users.Interfaces;
using CryptoStats.Businesslayer.Users.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.Extensions.Logging;

namespace CryptoStats.UI.Controllers.v1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class VerificationController : ControllerBase
    {
        private readonly IVerificationService _verificationService;
        private readonly IUserService _userService;
        private readonly IJwtService _jwtService;
        private readonly JwtOptions _jwtOptions;
        private readonly ILogger<VerificationController> _logger;

        public VerificationController(IVerificationService verificationService, IUserService userService, IJwtService jwtService, IOptions<JwtOptions> jwtOptions, ILogger<VerificationController> logger)
        {
            _verificationService = verificationService;
            _userService = userService;
            _jwtService = jwtService;
            _jwtOptions = jwtOptions.Value;
            _logger = logger;
        }

        [HttpPost("verificate")]
        [AllowAnonymous]
        public async Task<IActionResult> Verificate([FromBody] GetVerificationDTO verification)
        {
            if (await _verificationService.Check(verification))
            {
                GetUserDTO user = await _userService.GetById(verification.UserId);

                if (user == null)
                {
                    _logger.LogWarning(" User [" + user.Firstname + " | " + user.Lastname + "] not found");

                    return Unauthorized();
                }

                _logger.LogInformation(" User [" + user.Firstname + " | " + user.Lastname + "] verificated successfully");

                string token = _jwtService.CreateToken(user);

                user.JwtResponse = new JwtResponseDTO { Token = token, ExpiryDate = DateTime.Now.AddDays(_jwtOptions.TokenExpiryInDays) };

                // Delete Token
                await _verificationService.Delete(verification.UserId);

                _logger.LogInformation(" User [" + user.Firstname + " | " + user.Lastname + "] verification pin deleted");

                return Ok(user);
            }

            return Unauthorized();
        }

        [HttpPost("secret")]
        [AllowAnonymous]
        public async Task<IActionResult> CheckSecret([FromBody] SecretDTO secret)
        {
            if (await _verificationService.CheckSecret(secret))
            {
                GetUserDTO user = await _userService.GetById(secret.UserId);

                if (user == null)
                {
                    _logger.LogWarning(" User [" + user.Firstname + " | " + user.Lastname + "] not found");

                    return Unauthorized();
                }

                _logger.LogInformation(" User [" + user.Firstname + " | " + user.Lastname + "] verificated successfully");

                string token = _jwtService.CreateToken(user);

                user.JwtResponse = new JwtResponseDTO { Token = token, ExpiryDate = DateTime.Now.AddDays(_jwtOptions.TokenExpiryInDays) };

                // Delete Token
                await _verificationService.Delete(secret.UserId);

                return Ok(user);
            }

            return Unauthorized();
        }


        [HttpPost]
        [AllowAnonymous]
        public async void Create([FromBody] AddVerificationDTO verification)
        {
            await _verificationService.Create(verification);

            _logger.LogInformation(" Verification for user with id: [" + verification.UserId + "] created");

        }
    }
}
