﻿using CryptoStats.Businesslayer.Users.Models;
using FluentValidation;

namespace CryptoStats.Businesslayer.Users.Validators
{
    public class GetUserValidator : AbstractValidator<GetUserDTO>
    {
        public GetUserValidator()
        {
            RuleFor(x => x.Id).NotNull();
            RuleFor(x => x.Firstname).NotNull().MaximumLength(50);
            RuleFor(x => x.Lastname).NotNull().MaximumLength(50);
            RuleFor(x => x.Email).NotNull().EmailAddress();
            RuleFor(x => x.Phone).Matches("^(\\b(0041|0)|\\B\\+41)(\\s?\\(0\\))?(\\s)?[1-9]{2}(\\s)?[0-9]{3}(\\s)?[0-9]{2}(\\s)?[0-9]{2}\\b");
            RuleFor(x => x.Username).NotNull().MaximumLength(50);
            RuleFor(x => x.Secret).NotNull();
            RuleFor(x => x.Role).NotNull();
        }
    }
}
