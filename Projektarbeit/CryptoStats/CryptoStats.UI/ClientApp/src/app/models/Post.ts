export class Post {
    id: number;
    title: string;
    public: boolean;
    content: string;
    creator: string;
    userId: number;
    comments: Comment[];
}