import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from "rxjs";
import { catchError } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { JwtResponse } from '../models/JwtResponse';
import { Globals } from './globals';
import { User } from '../models/User';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptorProvider implements HttpInterceptor {
  constructor(private apiService: ApiService, private globals: Globals, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    const jwtResponse: JwtResponse = JSON.parse(localStorage.getItem("jwtResponse"))
    let token: string = "";
    let userId: number;
    if (jwtResponse) {
      token = jwtResponse.token;
    }

    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
    return next.handle(req).pipe(catchError(err => {
      if (err.status === 401) {
        if (!req.url.includes("verificate")) {
          localStorage.removeItem("jwtResponse");
          localStorage.removeItem("user");
          this.globals.loggedIn = false;
        }
        if (this.globals.user && !this.globals.selectedPost && !this.globals.selectedUser) {
          userId = this.globals.user.id;
          this.globals.user = new User();
          this.globals.user.id = userId;
        }

        if (req.url.includes("check")) {
          this.globals.error = "Username or password wrong!";
        }
        else if (req.url.includes("verificate")) {
          this.globals.error = "Verification wrong!";
        }
        else if (req.url.includes("secret")) {
          this.globals.error = "Secret wrong!";
        }
      }
      else if (err.status == 0) {
        this.globals.error = "No connection to api!"
      }
      else if (err.status == 400) {
        if (req.url.includes("check")) {
          this.globals.error = "Ur blocked!";
        }
      }

      let error;

      if (err.error) {
        error = err.error.message || err.statusText;
      }

      return error;
    }));
  }
}