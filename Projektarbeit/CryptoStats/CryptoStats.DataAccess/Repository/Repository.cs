﻿using CryptoStats.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace CryptoStats.DataAccess.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly CryptoStatsContext _dbContext;

        private const string entityIdentifierName = "Id";

        public Repository(CryptoStatsContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task<T> GetFirstMatching(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllMatchingAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>()
                .Where(predicate)
                .ToListAsync();
        }

        public async Task<int> InsertAsync(T entity)
        {
            var record = await _dbContext.Set<T>().AddAsync(entity);

            await _dbContext.SaveChangesAsync();

            if (record.Property(entityIdentifierName)?.CurrentValue is int id)
            {
                return id;
            }

            return -1;
        }

        public async Task UpdateAsync(T entity)
        {
            _dbContext.Set<T>().Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
    }
}
