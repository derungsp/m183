﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Authentication.Options;
using CryptoStats.Businesslayer.Authentication.Models;
using CryptoStats.Businesslayer.Users.Interfaces;
using Microsoft.AspNetCore.Authentication.Google;
using CryptoStats.Businesslayer.Users.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace CryptoStats.UI.Controllers.v1
{
    [Authorize(AuthenticationSchemes = GoogleDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _logger;
        private readonly IUserIdentityService _identityService;
        private readonly IUserService _userService;
        private readonly IJwtService _jwtService;
        private readonly JwtOptions _jwtOptions;

        public AuthenticationController(IJwtService jwtService, IUserService userService, IUserIdentityService identityService, IOptions<JwtOptions> jwtOptions, ILogger<AuthenticationController> logger)
        {
            _jwtService = jwtService;
            _userService = userService;
            _identityService = identityService;
            _jwtOptions = jwtOptions.Value;
            _logger = logger;
        }

        /// <summary>
        /// Generates a token if the user is in the databse => from basic authentication
        /// </summary>
        /// <returns>JWT token as IActionResult</returns>
        [HttpPost("basic")]
        [AllowAnonymous]
        public async Task<IActionResult> BasicAuthentication([FromBody] BasicAuthenticationDTO basicAuthenticationModel)
        {
            if (!await _userService.UsernameExists(basicAuthenticationModel.Username))
            {
                _logger.LogWarning(" User [" + basicAuthenticationModel.Username + "] tried to log in but wasn't found");

                return Conflict();
            }

            GetUserDTO user = await _userService.GetByBasicAuthentication(basicAuthenticationModel.Username, basicAuthenticationModel.Password);

            if (user.Blocked)
            {
                _logger.LogWarning(" User [" + basicAuthenticationModel.Username + "] tried to log in but was blocked");

                if (user.BlockDate > DateTime.Now.AddMinutes(-5))
                {
                    return NoContent();
                }
                else
                {
                    UpdateUserDTO updateUser = new UpdateUserDTO()
                    {
                        Id = user.Id,
                        BlockDate = user.BlockDate,
                        Blocked = user.Blocked,
                        InvalidLoginCount = user.InvalidLoginCount
                    };

                    await _userService.Update(updateUser);
                }
            }
            if (user == null)
            {
                _logger.LogWarning(" User [" + basicAuthenticationModel.Username + "] tried to log in but the password was wrong");

                return Unauthorized();
            }

            _logger.LogInformation(" User [" + user.Firstname + " | " + user.Lastname + "] logged in successfully");

            string token = _jwtService.CreateToken(user);

            return Ok(new JwtResponseDTO { Token = token, ExpiryDate = DateTime.Now.AddDays(_jwtOptions.TokenExpiryInDays) });
        }

        /// <summary>
        /// Checks if user exists in database and if the user is blocked for 5 mins
        /// </summary>
        /// <returns>CredentialsCheckResponseDTO as IActionResult</returns>
        [HttpPost("check")]
        [AllowAnonymous]
        public async Task<IActionResult> CredentialsCheck([FromBody] BasicAuthenticationDTO basicAuthenticationModel)
        {
            if (!await _userService.UsernameExists(basicAuthenticationModel.Username))
            {
                _logger.LogWarning(" User [" + basicAuthenticationModel.Username + "] tried to log in but wasn't found");

                return Unauthorized();
            }

            GetUserDTO user = await _userService.GetByBasicAuthentication(basicAuthenticationModel.Username, basicAuthenticationModel.Password);

            if (user == null)
            {
                _logger.LogWarning(" User [" + basicAuthenticationModel.Username + "] tried to log in but the credentials were wrong");

                return Unauthorized();
            }

            if (user.Id == -1)
            {
                if (user.Blocked)
                {
                    _logger.LogWarning(" User [" + basicAuthenticationModel.Username + "] tried to log in but was blocked");

                    if (user.BlockDate > DateTime.Now.AddMinutes(-5))
                    {
                        return BadRequest();
                    }
                    else
                    {
                        UpdateUserDTO updateUser = new UpdateUserDTO()
                        {
                            Id = user.Id,
                            BlockDate = null,
                            Blocked = false,
                            InvalidLoginCount = 0
                        };

                        await _userService.Update(updateUser);
                    }
                }
                else
                {
                    _logger.LogWarning(" User [" + basicAuthenticationModel.Username + "] tried to log in but was blocked");

                    return Unauthorized();
                }

                return BadRequest();
            }

            _logger.LogInformation(" User [" + user.Firstname + " | " + user.Lastname + "] was checked successfully");

            CredentialsCheckResponseDTO response = new CredentialsCheckResponseDTO() { UserId = user.Id, Phone = user.Phone };

            return Ok(response);
        }

        /// <summary>
        /// Generates a token if the user is in the database => from google authentication
        /// </summary>
        /// <returns>User with jwt Token</returns>
        [HttpGet("google")]
        public async Task<IActionResult> GoogleAuthentication()
        {
            string email = _identityService.GetClaims().ToList().SingleOrDefault(c => c.Type.Contains("email")).Value;

            if (!await _userService.EmailExists(email))
            {
                AddUserDTO userDTO = new AddUserDTO()
                {
                    Firstname = _identityService.GetClaims().ToList().SingleOrDefault(c => c.Type.Contains("givenname")).Value,
                    Lastname = _identityService.GetClaims().ToList().SingleOrDefault(c => c.Type.Contains("surname")).Value,
                    Email = email,
                    Username = email,
                    RoleId = 2
                };

                _logger.LogInformation("User [" + userDTO.Firstname + " | " + userDTO.Lastname + "] has been created after google authentication");

                await _userService.Create(userDTO);
            }

            GetUserDTO user = await _userService.GetByEmail(email);

            _logger.LogInformation("User [" + user.Firstname + " | " + user.Lastname + "] logged in successfully");

            string token = _jwtService.CreateToken(user);

            user.JwtResponse = new JwtResponseDTO { Token = token, ExpiryDate = DateTime.Now.AddDays(_jwtOptions.TokenExpiryInDays) };

            return Ok(user);
        }
    }
}