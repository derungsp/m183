﻿namespace CryptoStats.Businesslayer.Verifications.Models
{
    public class SecretDTO
    {
        public string Value { get; set; }

        public int UserId { get; set; }

    }
}
