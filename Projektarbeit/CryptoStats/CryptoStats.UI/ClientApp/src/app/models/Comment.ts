export class Comment {
    id: number;
    content: string;
    creator: string;
    postId: number;
    userId: number;
}