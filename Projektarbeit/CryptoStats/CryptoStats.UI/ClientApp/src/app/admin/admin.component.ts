import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from '../providers/globals';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html'
})
export class AdminComponent implements OnInit {

  constructor(public globals: Globals, private router: Router) { }

  ngOnInit(): void {

    if (!this.globals.loggedIn) {
      this.router.navigate(['/']);
    }
  }
}
