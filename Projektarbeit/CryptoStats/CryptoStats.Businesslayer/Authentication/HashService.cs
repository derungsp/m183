﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;
using System.Text;
using System;

namespace CryptoStats.Businesslayer.Authentication
{
    public class HashService : IHashService
    {
        /// <summary>
        /// Creates a hash out of a value and a random salt
        /// </summary>
        /// <param name="value">For example a password => string</param>
        /// <param name="salt">Random salt string</param>
        /// <returns>Base64 string</returns>
        public string CreateHash(string value, string salt)
        {
            var valueBytes = KeyDerivation.Pbkdf2(
                password: value,
                salt: Encoding.UTF8.GetBytes(salt),
                prf: KeyDerivationPrf.HMACSHA512,
                iterationCount: 10000,
                numBytesRequested: 256 / 8);

            return Convert.ToBase64String(valueBytes);
        }

        /// <summary>
        /// Checks if value equals the base64 hash string
        /// </summary>
        /// <param name="value">Plain text value</param>
        /// <param name="salt">Saved salt</param>
        /// <param name="hash">Saved hash</param>
        /// <returns></returns>
        public bool Validate(string value, string salt, string hash)
        {
            return CreateHash(value, salt) == hash;
        }

        /// <summary>
        /// Creates a random salt
        /// </summary>
        /// <returns>Salt as base64 string</returns>
        public string CreateSalt()
        {
            byte[] randomBytes = new byte[128 / 8];
            using (var generator = RandomNumberGenerator.Create())
            {
                generator.GetBytes(randomBytes);
                return Convert.ToBase64String(randomBytes);
            }
        }
    }
}
