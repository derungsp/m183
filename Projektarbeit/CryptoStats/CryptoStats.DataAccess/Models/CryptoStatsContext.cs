﻿using Microsoft.EntityFrameworkCore;
using System;

namespace CryptoStats.DataAccess.Models
{
    public class CryptoStatsContext : DbContext
    {
        //private static bool _created = false;

        public CryptoStatsContext(DbContextOptions<CryptoStatsContext> options) : base(options)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Entity framework core code first

            // Relationships:
            modelBuilder.Entity<Role>()
                .HasMany(x => x.Users).WithOne(x => x.Role).HasForeignKey(x => x.RoleId).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasOne(x => x.Role).WithMany(x => x.Users).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Posts).WithOne(x => x.User).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Post>()
                .HasOne(x => x.User).WithMany(x => x.Posts).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Comments).WithOne(x => x.User).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Comment>()
                .HasOne(x => x.User).WithMany(x => x.Comments).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Verifications).WithOne(x => x.User).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Verification>()
                .HasOne(x => x.User).WithMany(x => x.Verifications).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Post>()
                .HasMany(x => x.Comments).WithOne(x => x.Post).HasForeignKey(x => x.PostId).OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Comment>()
                .HasOne(x => x.Post).WithMany(x => x.Comments).OnDelete(DeleteBehavior.SetNull);

            // Default users and role seed
            modelBuilder.Entity<User>()
                .HasData(new User() { Id = 1, Firstname = "pascal", Lastname = "derungs", Email = "pascal6313@gmail.com", BlockDate = null, Blocked = false, InvalidLoginCount = 0, Phone = "0791359448", Username = "derungsp", Secret = "12345678901234567890", Active = true, Timestamp = DateTime.Now, PasswordHash = "k21Ozv9on6Rvl52gFQlpm4xFTEWhp1DWo4MvRYWZzJw=", PasswordSalt = "+BYBqFhKN3fG0+kZjWHOaQ==", RoleId = 1 });

            modelBuilder.Entity<User>()
                .HasData(new User() { Id = 2, Firstname = "max", Lastname = "muster", Email = "max.muster@gmail.com", BlockDate = null, Blocked = false, InvalidLoginCount = 0, Phone = "0791359448", Username = "maxmuster", Secret = "12345678901234567890", Active = true, Timestamp = DateTime.Now, PasswordHash = "k21Ozv9on6Rvl52gFQlpm4xFTEWhp1DWo4MvRYWZzJw=", PasswordSalt = "+BYBqFhKN3fG0+kZjWHOaQ==", RoleId = 2 });

            modelBuilder.Entity<Role>()
                .HasData(new Role() { Id = 1, Name = "Admin" });

            modelBuilder.Entity<Role>()
                .HasData(new Role() { Id = 2, Name = "User" });

            // Temp seeds for development
            modelBuilder.Entity<Post>()
                .HasData(new Post() { Id = 1, Title = "Post 1", Content = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", Timestamp = DateTime.Now, Deleted = false, Public = true, UserId = 1 });

            modelBuilder.Entity<Comment>()
                .HasData(new Comment() { Id = 1, Content = "test - content", Timestamp = DateTime.Now, PostId = 1, UserId = 1 });

            modelBuilder.Entity<Verification>()
                .HasData(new Verification() { Id = 1, Value = 232232, Timestamp = DateTime.Now, UserId = 1 });

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Verification> Verifications { get; set; }
    }
}
