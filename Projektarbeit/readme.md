# M183 Projektarbeit "CryptoStats"

Pascal Derungs

## Einleitung
### Installation
Zur Inbetriebnahme der Applikation öffnen Sie die Visual Studio Solution im Ordner "CryptoStats" und starten Sie das Projekt "CryptoStats.UI" als Developement. Anschliessend werden automatisch die nötigen node_modules installiert und die SQLite Datenbank generiert. Deshalb kann das erste Starten der Applikation etwas Zeit in Anspruch nehmen.

Die Applikation "CryptoStats" verwendet für das Backend eine .NET Core Webapplikation, welche in drei Layern aufgebaut ist.
### CryptoStats.DataAccess
Der Businesslayer ist auch eine C# Klassenbibliothek. Der Layer "DataAccess" ist die Schnittstelle zwischen Datenbank und der "Logik" im Businesslayer. Für den Datenbankzugriff habe ich Entityframework Core von Microsoft verwendet. Ausserdem habe ich ein Repository-Pattern verbaut, damit man unnötige Redundanzen vermeiden kann und die üblichen Datenbankzugriffe nicht für jede Entität erstellt werden muss.
### CryptoStats.Businesslayer
Der Businesslayer ist auch eine C# Klassenbibliothek. Im Businesslayer geschieht die ganze Logik des Backends. Für jede "Gebiet" (z.B. Posts, Users usw.) wurde ein Ordner erstellt, der jeweils alles zu diesem Bereich enthält. Für das einfache "Mapping" zwischen DTO's und den Entitäten in der Datenbank wurde ein AutoMapper verwendet. Für die Validation sind Validatoren mittels FluentValidation implementiert.
### CryptoStats.UI
Das UI beinhaltet die eigentliche Webapplikation. Er enthält alle Controller, die als Endpoints dienen und hat ausserdem im Ordner "ClientApp" die verwendete Angular App, welche für das Frontend verwendet wurde.

## 8.1 Verfahren zur Speicherung von Passwörtern
Für das sichere Speichern von Passwörtern verwende ich die Hashfunktion SHA512 in Verbindung mit einem generierten Salt. Der Hash-Wert und der Salt werden beide in die Datenbank gespeichert. Somit kann niemand das Passwort dadurch herausfinden. Zum Überprüfen des Passworts wird das eingegebene Passwort mit dem gespeicherten Salt gehasht und mit dem gespeicherten Hash verglichen. Ich habe die ganzen Funktionen zum Thema Hashing in einen eigenen Service ausgelagert (HashService).
## 8.2 Einsatz von Bibliotheken
### Bootstrap
Ich habe Bootstrap verwendet, damit ich im UI auf vordefinierte Elemente zugreifen kann und beim Design Zeit einsparen kann. Bootstrap ist einfach zu verwenden und hilft eine schöne und vorallem responsive Seite zu bauen.
### AutoMapper
Für das einfache "Mapping" zwischen DTO's und den Entitäten in der Datenbank wurde ein AutoMapper verwendet. Dazu müssen die verschiedenen Mapping definiert und eingebunden werden. Mit der Bibliothek AutoMapper konnte ich viel Zeit sparen, da ich das manuelle "mappen" vermeiden konnte.
### Fluent Validation
Für die Validierung im Backend habe ich FluentValidation eingebunden. Mithilfe dieser Bibliothek können für jede Entität Validatoren erstellt werden, welche für jedes Property die gewünschten Validationen definieren lässt. Ohne weiteren Code werden dann die Models bei jedem Gebrauch über den API selbständig Validiert.
### Angular
Für das Frontend habe ich Angular verwendet, da ich viel Erfahrung damit habe und ein sehr gutes Single-Page-Application Framework ist. Angular ist zudem gut, da es mittels Typescript Dateien auch Objektbasiert ist.
### Newtonsoft.Json
Ich verwende für die Kommunikation zwischen API und Client Daten als JSON und musste dadurch Newtonsoft.Json einbinden.
### NLog
Ich habe Nlog eingebunden, da es eine gute und einfache Bibliothek zur Erstellung von Logs ist.
### Jwt
Ich arbeite mit Jwt-Tokens für die Authentifikation. Dieses Token wird im Backend erstellt, wenn sich der Benutzer korrekt anmeldet und danach erhält er dieses Token, welches im LocalStorage abgespeichert wird. Dieses Token wird bei jedem Request auf den API mitgesendet und überprüft. Bekommt der API dann kein Token im Header gibt er einen 401-Error zurück.
### EntityFramework Core
Für die Datenbankverbindung und Kommunikation habe ich EF Core verwendet, da es alles mitbringt, was ich brauche.
### Dependency Injection
Für die erweiterte Unabhängigkeit meines Codes wurde Dependecy Injection verwendet, welche die Abhängigkeiten eines Objekts zur Laufzeit reglementiert. Diese Bibliothek gibt es direkt von Microsoft.

## 8.3 Schutz gegen Cross-Site-Scripting Attacken
Da man auf den Api ausschliesslich mit Jwt-Tokens zugreifen kann berufe ich mich auf den von Angular eingebauten Schutz gegen XSS-Attacken. Angular besitzt ein grosses Paket an Schutzmassnahmen gegen diverse Attacken unter anderem XSS-Attacken. Somit ist es nicht notwendig noch weitere Massnahmen vorzunehmen. Empfehlenswert ist lediglich die Angular Version immer aktuell zu halten.

## 8.4 Credentials
In meiner Applikation kann man die Rolle "User" oder die Rolle "Admin" besitzen.
Der Secret-Key wird für die Verifikation ohne Telefon gebraucht.
In der erstellten Applikation wurden zu den beiden Rollen Logindaten vordefiniert:

```
* Username: maxmuster | Passwort: test1234 Secret: 12345678901234567890                 => User
* Username: derungsp | Passwort: test1234 | Secret: 12345678901234567890                => Admin
```

## Login Konzept
Ich habe beim Login in Verwendung mit NLog mich darauf entschieden, nur die relevanten und wichtigen Ereignisse zu loggen. Dazu gehören alle Geschichten rund um die Authentifikation eines Users und die Erstellung bzw. Bearbeitung eines Posts, Users oder Kommentars. Die Logeinträge werden in Dateien im DataAccess abgelegt. Unter Logs/ werden alle selber getriggerten Logs gespeichert und unter internal_logs werden Systemlogs abgelegt. Ich habe die Logs bewusst nicht in die Datenbank abgelegt, da die SQLite Datenbank nicht für so grosse Datenspeicherung gedacht ist und meiner Erfahrung nach mit einer gewissen Grösse ziemlich unperformant wird.

## Google Authentication
Aus einem Problem, das nicht beheben worde konnte, muss für die Google Authentikation die Methode auf dem API beim ersten Mal direkt aufgerufen werden. Die URL dazu lautet https://localhost:44365/api/authentication/google. Anschliessend kann man via Button auf der Login Page eine Google Authentikation vornehmen.