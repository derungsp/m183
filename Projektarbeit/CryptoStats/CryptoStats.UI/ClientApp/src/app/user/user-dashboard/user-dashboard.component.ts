import { Component, NgModule, OnInit } from '@angular/core';
import { Post } from 'src/app/models/Post';
import { ApiService } from 'src/app/services/api.service';
import { Globals } from 'src/app/providers/globals';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html'
})
// @NgModule({
//   imports: [CommonModule],
//   declarations: [UserDashboardComponent]
// })

export class UserDashboardComponent implements OnInit {
  posts: Post[];

  constructor(
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.globals.loggedIn) {
      this.router.navigate(['/']);
    }
    else{
      if (this.globals.user.role.name == "Admin") {
        this.router.navigate(['/admin/dashboard']);
      }
      this.apiService.getOwnPosts().subscribe(res=>this.posts=res);
    }
  }

  addPost(){
    this.globals.selectedPost = new Post();
    this.router.navigate(['/post/edit']);
  }

  deletePost(post: Post) {
    this.posts = this.apiService.deletePost(post);
  }

  selectPost(post){
    this.globals.selectedPost = post;
    this.router.navigate(['/post/edit']);
  }

  public open(event, item) {
    this.globals.selectedPost = item;
    this.router.navigate(['/post']);
  }
}
