﻿namespace CryptoStats.Businesslayer.Comments.Models
{
    public class AddCommentDTO
    {
        public string Content { get; set; }

        public int PostId { get; set; }

        public int UserId { get; set; }

    }
}
