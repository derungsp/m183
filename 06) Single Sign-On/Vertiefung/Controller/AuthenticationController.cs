﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Authentication.Models;
using CryptoStats.Businesslayer.Logins.Interfaces;
using Microsoft.AspNetCore.Authentication.Google;
using CryptoStats.Businesslayer.Users.Interfaces;
using CryptoStats.Businesslayer.Logins.Models;
using CryptoStats.Businesslayer.Users.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace CryptoStats.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = GoogleDefaults.AuthenticationScheme)]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly ILogger<AuthenticationController> _loggerService;
        private readonly IUserIdentityService _userIdentityService;
        private readonly ILoginService _loginService;
        private readonly IUserService _userService;
        private readonly IJwtService _jwtService;

        public AuthenticationController(IJwtService jwtService, IUserService userService, IUserIdentityService userIdentityService, ILogger<AuthenticationController> loggerService, ILoginService loginService)
        {
            _userIdentityService = userIdentityService;
            _loggerService = loggerService;
            _loginService = loginService;
            _userService = userService;
            _jwtService = jwtService;
        }

        [HttpGet("google")]
        public async Task<IActionResult> Google()
        {
            UserDTO user = await _userService.GetByEmail(_userIdentityService.GetClaims().ToList().SingleOrDefault(c => c.Type.Contains("email")).Value);

            if (user == null)
            {
                _loggerService.LogWarning(_userIdentityService.GetClaims().ToList().SingleOrDefault(c => c.Type.Contains("email")).Value + " unauthorized request");
                return Unauthorized();
            }

            await _loginService.Create(new LoginDTO { Time = DateTime.Now, UserId = user.Id });
            _loggerService.LogInformation("User [" + user.Username + "] logged in");

            return Ok(_jwtService.CreateToken(user));
        }

        [HttpPost("basic")]
        [AllowAnonymous]
        public async Task<IActionResult> Basic(BasicAuthenticationDTO model)
        {
            UserDTO user = await _userService.GetByBasicAuthentication(model.Identifier, model.Password);

            if (user == null)
            {
                _loggerService.LogWarning("unauthorized request", model);
                return Unauthorized();
            }

            user.Jwt = _jwtService.CreateToken(user);

            await _loginService.Create(new LoginDTO { Time = DateTime.Now, UserId = user.Id });
            _loggerService.LogInformation("User [" + user.Username + "] logged in");

            return Ok(user);
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register(UserDTO userDTO)
        {
            bool emailExists = await _userService.EmailCheck(userDTO.Email);

            if (!emailExists)
            {
                UserDTO user = await _userService.Create(userDTO);

                _loggerService.LogInformation("User [" + userDTO.Username + "] registered");
                await _loginService.Create(new LoginDTO { Time = DateTime.Now, UserId = user.Id });

                return Ok(await _userService.GetByEmail(userDTO.Email));
            }
            else
            {
                _loggerService.LogError("E-Mail [" + userDTO.Email + "] existed");
                return BadRequest("E-Mail exists");
            }
        }
    }
}