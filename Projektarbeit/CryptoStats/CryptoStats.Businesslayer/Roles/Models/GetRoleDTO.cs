﻿namespace CryptoStats.Businesslayer.Roles.Models
{
    public class GetRoleDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }
}
