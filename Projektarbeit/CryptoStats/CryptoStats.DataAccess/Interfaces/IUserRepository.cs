﻿using CryptoStats.DataAccess.Repository;
using CryptoStats.DataAccess.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;

namespace CryptoStats.DataAccess.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetFirstMatchingUserWithRole(Expression<Func<User, bool>> predicate);

        Task<IEnumerable<User>> GetAllUsersWithRole();
    }
}
