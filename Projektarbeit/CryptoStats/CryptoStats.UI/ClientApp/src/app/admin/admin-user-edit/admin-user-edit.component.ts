import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from 'src/app/models/Role';
import { Globals } from 'src/app/providers/globals';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-admin-user-edit',
  templateUrl: './admin-user-edit.component.html'
})
export class AdminUserEditComponent implements OnInit {
  roles: Role[];
  constructor(private apiService: ApiService, public globals: Globals, private router: Router) { }

  ngOnInit(): void {
    if (!this.globals.selectedUser) {
      this.router.navigate(['/admin/dashboard']);
    }
    else{
      this.apiService.getAllRoles().subscribe(res=>this.roles=res);
    }
  }

  onChangeObj(newObj) {
    this.globals.selectedUser.role = newObj as Role;
  }
}
