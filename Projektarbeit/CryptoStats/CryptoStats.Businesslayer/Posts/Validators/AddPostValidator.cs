﻿using CryptoStats.Businesslayer.Posts.Models;
using FluentValidation;

namespace CryptoStats.Businesslayer.Posts.Validators
{
    public class AddPostValidator : AbstractValidator<AddPostDTO>
    {
        public AddPostValidator()
        {
            RuleFor(x => x.Title).NotNull();
            RuleFor(x => x.Content).NotNull();
            RuleFor(x => x.Public).NotNull();
            RuleFor(x => x.Timestamp).NotNull();
            RuleFor(x => x.UserId).NotNull();
        }
    }
}
