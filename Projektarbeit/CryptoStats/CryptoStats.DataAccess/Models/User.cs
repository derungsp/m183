﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;

namespace CryptoStats.DataAccess.Models
{
    [Table("User")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Username { get; set; }

        public string Secret { get; set; }

        public string PasswordHash { get; set; }

        public string PasswordSalt { get; set; }

        public bool Active { get; set; }

        public DateTime Timestamp { get; set; }

        public int InvalidLoginCount { get; set; }

        public bool Blocked { get; set; }

        public DateTime? BlockDate { get; set; }

        public int RoleId { get; set; }

        public virtual Role Role { get; set; }

        public virtual List<Post> Posts { get; set; }

        public virtual List<Comment> Comments { get; set; }

        public virtual List<Verification> Verifications { get; set; }

    }
}
