﻿using CryptoStats.Businesslayer.Verifications.Models;
using System.Threading.Tasks;

namespace CryptoStats.Businesslayer.Verifications.Interfaces
{
    public interface IVerificationService
    {
        Task<GetVerificationDTO> GetById(int id);

        Task<GetVerificationDTO> GetByUserId(int userId);

        Task<bool> Check(GetVerificationDTO verification);

        Task<bool> CheckSecret(SecretDTO secret);

        Task Create(AddVerificationDTO verification);

        Task Delete(int userId);

    }
}
