import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/Post';
import { Globals } from 'src/app/providers/globals';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html'
})
export class PostEditComponent implements OnInit {
  post: Post;
  postForm: FormGroup;
  submitted = false;
  // basicAuthenticationModel = new BasicAuthenticationModel();

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.globals.selectedPost && !this.globals.user) {
      this.router.navigate(['/']);
    }
    else{
      this.post = this.globals.selectedPost;
    }

    if (!this.globals.selectedPost) {
      this.router.navigate(['/' + this.globals.user.role.name.toLowerCase() + "/dashboard"]);
    }
    
    if (!this.globals.loggedIn) {
      this.router.navigate(['/']);
    }

    if (this.post) {
      if (!this.post.id) {
        this.post.public = false;
      }
    }

    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', [Validators.required]],
      public: [false, [Validators.required]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.postForm.controls; }

  onSubmit() {
    this.globals.error = null;
    this.submitted = true;

    // stop here if form is invalid
    if (this.postForm.invalid) {
      return;
    }

    this.post.title = this.postForm.value.title;
    this.post.content = this.postForm.value.content;
    this.post.public = this.postForm.value.public;
    this.post.userId = this.globals.user.id;

    // Process checkout data here
    this.postForm.reset();

    this.globals.tempPost = this.post;

     this.apiService.postChangeVerification(this.post);
  }
}
