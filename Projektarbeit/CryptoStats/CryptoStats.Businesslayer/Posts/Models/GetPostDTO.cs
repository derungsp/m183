﻿using CryptoStats.Businesslayer.Comments.Models;
using System.Collections.Generic;
using System;

namespace CryptoStats.Businesslayer.Posts.Models
{
    public class GetPostDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public string Creator { get; set; }

        public bool Public { get; set; }

        public bool Deleted { get; set; }

        public DateTime Timestamp { get; set; }

        public int UserId { get; set; }

        public IEnumerable<GetCommentDTO> Comments { get; set; }

    }
}
