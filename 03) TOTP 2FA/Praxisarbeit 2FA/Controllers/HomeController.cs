﻿using System;
using System.Threading.Tasks;
using Google.Authenticator;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Praxisarbeit_2FA.Data;
using Microsoft.Extensions.Configuration;
using Praxisarbeit_2FA.Models;

namespace Praxisarbeit_2FA.Controllers
{
    public class HomeController : Controller
    {
        private readonly M183DbContext _context;
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;

        public HomeController(M183DbContext context, ILogger<HomeController> logger, IConfiguration iConfig)
        {
            _context = context;
            _logger = logger;
            _configuration = iConfig;

            context.Database.EnsureCreated();
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] string username, [FromForm] string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
            if (user == null)
            {
                ViewBag.Message = "Wrong Credentials";
                ViewBag.AlertType = "danger";
                return View();
            }

            if (user.Password == password)
            {
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                var setupInfo = tfa.GenerateSetupCode("PRAXISARBEIT 2FA", username, user.Id.ToString(), false, 300);

                string qrCodeImageUrl = _configuration.GetValue<string>("TotpOptions:QrCodeSetupImageUrl");
                string manualEntrySetupCode = _configuration.GetValue<string>("TotpOptions:ManualEntryKey");
                
                return View("EnableAuthenticator", new TotpOptions { QrCodeSetupImageUrl = setupInfo.QrCodeSetupImageUrl, ManualEntryKey = setupInfo.ManualEntryKey, Username = username});
            }

            // User found but incorrect passwort
            ViewBag.Message = "Wrong Credentials";
            ViewBag.AlertType = "danger";
            return View();
        }

        public async Task<IActionResult> TOTP([FromForm]string inputCode, [FromForm]string username)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            bool isCorrectPIN = tfa.ValidateTwoFactorPIN(user.Id.ToString(), inputCode);

            if(isCorrectPIN)
            {
                return View("UserProfile");
            }
            else
            {
                ViewBag.Message = "Wrong Code";
                ViewBag.AlertType = "danger";
                return View("Login");
            }
        }

        public async Task<IActionResult> UserProfile(Guid userId)
        {
            var user = await _context.Users.FindAsync(userId);
            ViewBag.Username = user.Username;
            return View();
        }

    }
}