import { JwtResponse } from './JwtResponse';
import { Role } from './Role';

export class User {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    username: string;
    phone: string;
    verified: boolean;
    accounts:any;
    roleId: number;
    secret: string;
    role: Role;
    jwt: JwtResponse;
    password: string;
    waitingForVerification: boolean;
    jwtResponse: JwtResponse;
    active: boolean;
    timestamp: Date;
}