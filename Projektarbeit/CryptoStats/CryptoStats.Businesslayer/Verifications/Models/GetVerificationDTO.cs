﻿namespace CryptoStats.Businesslayer.Verifications.Models
{
    public class GetVerificationDTO
    {
        public int Id { get; set; }

        public int Value { get; set; }

        public int UserId { get; set; }

    }
}
