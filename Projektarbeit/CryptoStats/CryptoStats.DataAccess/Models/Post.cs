﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;

namespace CryptoStats.DataAccess.Models
{
    [Table("Post")]
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public bool Public { get; set; }

        public bool Deleted { get; set; }

        public DateTime Timestamp { get; set; }

        public int UserId { get; set; }

        public virtual List<Comment> Comments { get; set; }

        public virtual User User { get; set; }

    }
}
