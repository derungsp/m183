﻿using System;

namespace CryptoStats.Businesslayer.Authentication.Models
{
    /// <summary>
    /// Configuration options for JWT tokens
    /// </summary>
    public class JwtResponseDTO
    {
        public string Token { get; set; }

        public DateTime ExpiryDate { get; set; }

    }
}
