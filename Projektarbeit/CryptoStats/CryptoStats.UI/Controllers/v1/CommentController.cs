﻿using CryptoStats.Businesslayer.Comments.Interfaces;
using CryptoStats.Businesslayer.Comments.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoStats.UI.Controllers.v1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly ILogger<CommentController> _logger;

        public CommentController(ICommentService commentService, ILogger<CommentController> logger)
        {
            _commentService = commentService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IEnumerable<GetCommentDTO>> Create(AddCommentDTO comment)
        {
            await _commentService.Create(comment);

            _logger.LogInformation("Comment to post(" + comment.PostId + ")" + "from user(" + comment.UserId + ") created");

            return await _commentService.GetAllCommentsToPost(comment.PostId);
        }
    }
}
