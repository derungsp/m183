import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Secret } from 'src/app/models/Secret';
import { Globals } from 'src/app/providers/globals';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-secret',
  templateUrl: './secret.component.html'
})
export class SecretComponent implements OnInit {
  secretForm: FormGroup;
  submitted = false;
  secretModel = new Secret();
  userId;


  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.globals.user) {
      this.router.navigate(['/']);
    }
    else{
      if (!this.globals.user.id) {
        this.router.navigate(['/']);
      }
    }
    this.globals.error = null;
    this.globals.user = JSON.parse(localStorage.getItem("user"));
    this.secretForm = this.formBuilder.group({
      secret: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(20)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.secretForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.secretForm.invalid) {
      return;
    }

    if (this.globals.user) {
      this.secretModel.userId = this.globals.user.id;
      this.secretModel.value = this.secretForm.value.secret;
    }

    // Process checkout data here
    this.secretForm.reset();

    this.apiService.checkSecret(this.secretModel);
  }
}
