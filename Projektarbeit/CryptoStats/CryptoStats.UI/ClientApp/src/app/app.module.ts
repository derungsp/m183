import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { Globals } from './providers/globals';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { VerificationComponent } from './verification/verification.component';
import { TokenInterceptorProvider } from './providers/tokenInterceptor';
import { PostComponent } from './post/post.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { UserDashboardComponent } from './user/user-dashboard/user-dashboard.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { PostEditComponent } from './post/post-edit/post-edit.component';
import { AdminUserEditComponent } from './admin/admin-user-edit/admin-user-edit.component';
import { SecretComponent } from './verification/secret/secret.component';
import { UserEditVerificationComponent } from './user/user-edit/user-edit-verification/user-edit-verification.component';
import { PostEditVerificationComponent } from './post/post-edit/post-edit-verification/post-edit-verification.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    VerificationComponent,
    PostComponent,
    AdminComponent,
    UserComponent,
    UserEditComponent,
    PostEditComponent,
    AdminUserEditComponent,
    SecretComponent,
    UserEditVerificationComponent,
    PostEditVerificationComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'verification', component: VerificationComponent },
      { path: 'verification/secret', component: SecretComponent },
      { path: 'post', component: PostComponent },
      { path: 'post/edit', component: PostEditComponent },
      { path: 'user/dashboard', component: UserDashboardComponent },
      { path: 'admin/dashboard', component: AdminDashboardComponent },
      { path: 'admin/user/edit', component: AdminUserEditComponent },
      { path: 'user', component: UserComponent },
      { path: 'user/edit', component: UserEditComponent },
      { path: 'user/edit/verification', component: UserEditVerificationComponent },
      { path: 'post/edit/verification', component: PostEditVerificationComponent }
    ], { relativeLinkResolution: 'legacy' })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorProvider,
      multi: true
    },
    Globals
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
