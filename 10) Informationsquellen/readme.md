# Aufgabe Informationsquellen

Pascal Derungs

## Quellen

### Hacker Combat
[Hacker Combat](https://hackercombat.com/) ist eine Nachrichtenseite, die als Informationsquelle für IT-Sicherheitsexperten dient. Hacker Combat teilt seit 2017 IT-Expertenberatung und Einblicke, eingehende Analysen und Neuigkeiten rund um IT-security.

Als ein normaler Nutzer dieser Webseite finde ich die Themen und Wortwahl zu Fachspezifisch und unverständlich, da es tief in die Informatik eintaucht. Ausserdem wird diese Seite nur auf Englisch angeboten.
Aus der Perspektive eines Applikationsentwickler finde ich Hacker Combat sehr hilfreich und informativ. Sie ist einfach aufgebaut und listet auf der Startseite interessante und aktuelle Themen.

Ich finde die Inhalte der Webseite sind rein technisch und sehr fachspezifisch. Man findet oft klein gehaltene aber sehr informative Texte.

#### Ausgewählter Inhalt - "5 ways businesses can avoid phishing scams online"
In diesem Inhalt geht es um das Thema phishing scams speziell auf Firmen, welche seit der Corona Pandemie sehr verbreitet wurde. Es werden 5 Punkte erwähnt, wie man sich selber dagagen schützen kann. Es wird empfohlen immer mit einer VPN-Verbindung zu arbeiten, um verschlüsselt im Netz unterwegs zu sein. Der zweite Ratschlag ist, dass man mit seinen persönlichen Daten vorsichtig umgehen soll und schauen soll, ob der Absender bekannt oder unbekannt ist. Ausserdem gibt es immer mehr so genannte Pop-Up-Scams, die einen auf eine andere Seite weiterleiten will oder direkt nach sensitiven Daten verlangt. Es wird auch empfohlen immer auf sicheren Webseiten unterwegs zu sein, also immer mit dem HTTPS-Protokoll.

Ich persönlich fand diesen Artikel nicht sehr hilfreich, weil ich das meiste bereits in Trainings in meiner Firma bereits gelernt habe. Aber ich denke es ist ein guter, kurzer und hilfreicher Input für Leser, die sich noch nicht so mit dem Thema auseinander gesetzt haben.

### Netzwelt
[Netzwelt](https://www.spiegel.de/netzwelt/) ist eine Unterseite von der deutschen Nachrichtenfirma Spiegel. Spiegel Netzwelt veröffentlicht Beiträge rund um Technik und Digitales der ganzen Welt.

Diese Webseite spricht ein weites Spektrum an Leser an, da die Inhalte sehr einfach geschrieben sind und um simple News im IT-Bereich handelt, aber nicht sehr tief in die Materie geht.
Als Applikationsentwickler finde ich die Themen ziemlich oberflächlich gehalten und nicht sehr detailliert.

Die Netzwelt Inhalte sind aktuell und allgemein verständlich für die meisten Leser. Die Schreibweise ist auch unterhaltsamer als zum Beispiel bei Hacker Combat.

#### Ausgewählter Inhalt - "Cyberangriff auf Mitteldeutschen Rundfunk"
Dieser Beitrag handelt von einer DDoS-Angriff, welche am 07.11.2020 auf den Mitteldeutschen Rundfunk ausgeübt wurde. Von IP-Adressen aus der ganzen Welt wurden sehr viele Anfragen auf die Webseite gemacht, welche zu Problemen geführt haben. Für eine kurze Zeit konnten keine neuen Beiträge auf der Webseite veröffentlicht werden. Es wurden nicht sehr viele Informationen zum Angriff geteilt, jedoch wird untersucht, ob der Angriff einen Zusammenhang mit der "Querdenken"-Demo hat, welche zur gleichen Zeit war.

Der Beitrag war sehr oberflächlich gehalten und berichtete einfach, dass eine DDoS-Attacke stattgefunden hat. Mich hat verwundert, dass solche Attacken heutzutage noch so möglich sind.

### Infosecurity Magazine
Das [Infosecurity Magazine](https://www.infosecurity-magazine.com/) ist Teil der Infosecurity Group, welche eine Geschäftseinheit von Reed Exhibitions UK Ltd. Diverse Inhalte wurden bereits mehrfach preisgekrönt und beziehen sich auf Themen wie Trends, Nachrichtenanalysen und Meinungssäulen von Brachenexperten.

Als normaler Benutzer dieser Webseite bin ich nur bei gewissen Beiträgen angesprochen, da die Texte oftmals fachspezifisch sind.
Als Applikationsentwickler fühle ich mich sehr wohl auf "Inforsecurity Magazine", weil es auf spannende Themen eingeht und sehr informativ berichtet.

Die Schreibweise sind meist fachspezifisch und für viele nicht sehr verständlich. Die Texte sind aber sehr gut geschrieben und enthalten in kurzer Form die nötigen Informationen.

#### Ausgewählter Inhalt - "Most Americans Reuse Passwords for Work Devices"
Im Beitrag mit dem Titel "Most Americans Reuse Passwords for Work Devices" geht es um neue Untersuchungen in den USA. Es wurde herausgefunden, dass viele ihre privaten Passwörter auch auf Konton von Arbeitsgeräten verwenden. Und somit steigt die Anfälligeit für Cyber-Angriffe um einiges. Durch Befragungen wurde aber herausgefunden, dass sich nur rund 37% Gedanken über dieses Thema machen und sich um die Datensicherheit kümmern. Die meisten Mitarbeiter schieben die Verantwortlickeit auf den Arbeitgeber und hoffen, dass dieser alles für sie regelt. Ausserdem ist ein grosses Problem, dass viele Mitarbeiter das gleiche Passwort für all ihre Konten verwenden.

Ich fand diesen Beitrag spannend, weil man Resultate von vielen Umfragen und Untersuchungen sehen konnte und man so sehen konnte, wo zurzeit noch eines der grössten Sicherheitsproblem von Firmen und auch Privatpersonen ist.