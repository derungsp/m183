﻿using CryptoStats.Businesslayer.Authentication.Models;
using CryptoStats.Businesslayer.Roles.Models;
using CryptoStats.DataAccess.Models;
using System;

namespace CryptoStats.Businesslayer.Users.Models
{
    public class GetUserDTO
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Username { get; set; }

        public string Secret { get; set; }

        public bool Active { get; set; }

        public int InvalidLoginCount { get; set; }

        public bool Blocked { get; set; }

        public DateTime? BlockDate { get; set; }

        public GetRoleDTO Role { get; set; }

        public JwtResponseDTO JwtResponse { get; set; }

        public DateTime Timestamp { get; set; }

    }
}
