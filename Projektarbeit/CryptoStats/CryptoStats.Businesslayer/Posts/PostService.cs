﻿using CryptoStats.Businesslayer.Users.Interfaces;
using CryptoStats.Businesslayer.Posts.Interfaces;
using CryptoStats.Businesslayer.Comments.Models;
using CryptoStats.Businesslayer.Posts.Models;
using CryptoStats.DataAccess.Interfaces;
using CryptoStats.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using System;
using CryptoStats.Businesslayer.Authentication.Interfaces;
using CryptoStats.Businesslayer.Users.Models;

namespace CryptoStats.Businesslayer.Posts
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IUserIdentityService _userIdentityService;

        public PostService(IPostRepository postRepository, IMapper mapper, IUserService userService, IUserIdentityService userIdentityService)
        {
            _userService = userService;
            _postRepository = postRepository;
            _mapper = mapper;
            _userIdentityService = userIdentityService;
        }

        public async Task<IEnumerable<GetPostDTO>> GetAllPublicPosts()
        {
            IEnumerable<GetPostDTO> posts = _mapper.Map<IEnumerable<GetPostDTO>>(await _postRepository.GetAllMatchingPostsWithComments(x => x.Public && x.Deleted == false));

            foreach (GetPostDTO post in posts)
            {
                post.Creator = await _userService.GetUsername(post.UserId);

                foreach (GetCommentDTO c in post.Comments)
                {
                    c.Creator = await _userService.GetUsername(c.UserId);
                }
            }

            return posts;
        }

        public async Task<IEnumerable<GetPostDTO>> GetAllPosts()
        {
            IEnumerable<GetPostDTO> posts = _mapper.Map<IEnumerable<GetPostDTO>>(await _postRepository.GetAllAsync());

            foreach (GetPostDTO post in posts)
            {
                post.Creator = await _userService.GetUsername(post.UserId);

                foreach (GetCommentDTO c in post.Comments)
                {
                    c.Creator = await _userService.GetUsername(c.UserId);
                }
            }

            return posts;
        }

        public async Task<IEnumerable<GetPostDTO>> GetOwnPosts()
        {
            int id = _userIdentityService.GetId();

            IEnumerable<GetPostDTO> posts = _mapper.Map<IEnumerable<GetPostDTO>>(await _postRepository.GetAllMatchingPostsWithComments(x => x.UserId == id && x.Deleted == false));

            foreach (GetPostDTO post in posts)
            {
                post.Creator = await _userService.GetUsername(post.UserId);

                foreach (GetCommentDTO c in post.Comments)
                {
                    c.Creator = await _userService.GetUsername(c.UserId);
                }
            }

            return posts;
        }

        public async Task Create(AddPostDTO post)
        {
            post.Timestamp = DateTime.Now;

            await _postRepository.InsertAsync(_mapper.Map<Post>(post));
        }

        public async Task<IEnumerable<GetPostDTO>> Update(UpdatePostDTO post)
        {
            int id = _userIdentityService.GetId();

            await _postRepository.UpdateAsync(_mapper.Map<Post>(post));

            return _mapper.Map<IEnumerable<GetPostDTO>>(await _postRepository.GetAllMatchingPostsWithComments(x => x.UserId == id));
        }

        public async Task<IEnumerable<GetPostDTO>> Delete(GetPostDTO post)
        {
            int id = _userIdentityService.GetId();

            post.Deleted = true;

            await _postRepository.UpdateAsync(_mapper.Map<Post>(post));

            GetUserDTO user = await _userService.GetById(id);

            if (user.Role.Name == "Admin")
            {
                return await GetAllPosts();
            }
            else
            {
                return await GetOwnPosts();
            }

        }

        public async Task<IEnumerable<GetPostDTO>> RevertDeletion(GetPostDTO post)
        {
            int id = _userIdentityService.GetId();

            post.Deleted = false;

            await _postRepository.UpdateAsync(_mapper.Map<Post>(post));

            GetUserDTO user = await _userService.GetById(id);

            if (user.Role.Name == "Admin")
            {
                return await GetAllPosts();
            }
            else
            {
                return await GetOwnPosts();
            }

        }
    }
}
