using System;

namespace Praxisarbeit_2FA.Models
{
    public class TotpOptions
    {
        public string ManualEntryKey { get; set; }
        public string QrCodeSetupImageUrl { get; set; }
        public string Username { get; set; }
    }
}
