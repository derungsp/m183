﻿using CryptoStats.Businesslayer.Authentication.Interfaces;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Linq;
using System;

namespace CryptoStats.Businesslayer.Authentication
{
    public class UserIdentityService : IUserIdentityService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserIdentityService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Returns all claims of the User.Identity (ClaimsIdentity) of the current <see cref="HttpContext"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Claim> GetClaims()
        {
            if (_httpContextAccessor?.HttpContext?.User?.Identity is ClaimsIdentity identity)
            {
                return identity.Claims;
            }

            return new List<Claim>();
        }

        /// <summary>
        /// Returns the id of the current user
        /// </summary>
        /// <returns>0 if current user doesn't have a claim which sepcifies his id</returns>
        public int GetId()
        {
            // Application specific Id of the user is stored only in a jwt token => it isn't registered in the windows identity
            if (_httpContextAccessor?.HttpContext?.User?.Identity is ClaimsIdentity identity)
            {
                return Convert.ToInt32(identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value);
            }

            return default;
        }
    }
}
