﻿namespace CryptoStats.Businesslayer.Authentication.Models
{
    public class BasicAuthenticationDTO
    {
        public string Username { get; set; }

        public string Password { get; set; }

    }
}
