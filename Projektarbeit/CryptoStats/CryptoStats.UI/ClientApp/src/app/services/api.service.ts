import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BasicAuthenticationModel } from '../models/BasicAuthenticationModel';
import { CredentialsCheckResponse } from '../models/CredentialsCheckResponse';
import { JwtResponse } from '../models/JwtResponse';
import { Post } from '../models/Post';
import { Role } from '../models/Role';
import { Secret } from '../models/Secret';
import { User } from '../models/User';
import { Verification } from '../models/Verification';
import { Globals } from '../providers/globals';
import { SmsService } from './sms.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  //apiURL: string = 'https://localhost:44365/api/';
  apiURL: string = 'https://' + window.location.host + '/api/';
  jwtResponse: any;
  credentialsCheckResponse: CredentialsCheckResponse;
  date: Date;
  user: User;

  constructor(private httpClient: HttpClient, private router: Router, private smsService: SmsService, private globals: Globals) { }

  public getUserByEmail(email?: string): Observable<User> {
    return this.httpClient.get<User>(`${this.apiURL}user/${email}`);
  }

  public getUserById(id?: string): Observable<User> {
    return this.httpClient.get<User>(`${this.apiURL}user/${id}`);
  }

  public getAllPublicPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(`${this.apiURL}post`);
  }

  public getOwnPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(`${this.apiURL}post/own`);
  }

  // if admin
  public getAllPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(`${this.apiURL}post/admin`);
  }

  public getAllRoles(): Observable<Role[]> {
    return this.httpClient.get<Role[]>(`${this.apiURL}role`);
  }

  public getAllUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.apiURL}user`);
  }

  public getCommentToPost(postId): Observable<Comment[]> {
    return this.httpClient.get<Comment[]>(`${this.apiURL}comment/` + postId);
  }

  public addComment(comment): void {
    this.httpClient.post<Comment[]>(`${this.apiURL}comment`, comment, { observe: 'response' }).subscribe(response => {
      if (response.status == 200) {
        this.globals.selectedPost.comments = response.body;
      }
    });
  }

  public register(user: User): void {
    this.httpClient.post<User>(`${this.apiURL}user`, user, { observe: 'response' }).subscribe(response => {
      if (response.status == 200) {
        this.user = response.body;
        this.globals.user = this.user;
        this.globals.loggedIn = true;
        localStorage.setItem('jwtResponse', JSON.stringify(this.user.jwtResponse));
        localStorage.setItem('user', JSON.stringify(this.user));
        this.router.navigate(['/' + this.user.role.name.toLowerCase() + '/dashboard']);
      }
      else if (response.status == 204) {
        this.globals.error = "Username, E-Mail or phone already exists";
      }
    });
  }

  public addPost(post: Post): void {
    if (post.id) {
      this.httpClient.put<Post[]>(`${this.apiURL}post`, post, { observe: 'response' }).subscribe(response => {
        if (response.status == 200) {
          this.globals.posts = response.body
          this.router.navigate(['/user/dashboard']);
        }
      });
    }
    else {
      this.httpClient.post<Post[]>(`${this.apiURL}post`, post, { observe: 'response' }).subscribe(response => {
        if (response.status == 200) {
          this.globals.posts = response.body
          this.router.navigate(['/user/dashboard']);
        }
      });
    }
  }

  phoneChangeVerification(user: User) {
    let pin = this.smsService.send(user.phone);

    if (pin) {
      let verification = new Verification();
      verification.value = pin;
      verification.userId = user.id

      this.httpClient.post<Verification>(`${this.apiURL}verification`, verification, { observe: 'response' }).subscribe(response => {
        this.router.navigate(['/user/edit/verification']);
      });
    }
    else {
      this.globals.error = "SMS Gateway error!"
    }
  }

  postChangeVerification(post: Post) {
    let pin = this.smsService.send(this.globals.user.phone);

    if (pin) {
      let verification = new Verification();
      verification.value = pin;
      verification.userId = this.globals.user.id

      this.httpClient.post<Verification>(`${this.apiURL}verification`, verification, { observe: 'response' }).subscribe(response => {
        this.router.navigate(['/post/edit/verification']);
      });
    }
    else {
      this.globals.error = "SMS Gateway error!"
    }
  }

  public verificatePhoneChangeVerification(verificationModel: Verification): void {
    this.httpClient.post<User>(`${this.apiURL}verification/verificate`, verificationModel, { observe: 'response' }).subscribe(response => {
      this.updateUser(this.globals.tempUser);
    });
  }

  public updateUser(user: User): void {
    this.httpClient.put<User>(`${this.apiURL}user`, user, { observe: 'response' }).subscribe(response => {
      if (response.status == 200) {
        this.user = response.body;
        this.globals.user = this.user;
        this.globals.loggedIn = true;
        localStorage.setItem('jwtResponse', JSON.stringify(this.user.jwtResponse));
        localStorage.setItem('user', JSON.stringify(this.user));
        this.router.navigate(['/' + this.user.role.name.toLowerCase() + '/edit']);
      }
    });
  }

  public verificatePostChangeVerification(verificationModel: Verification): void {
    this.httpClient.post<User>(`${this.apiURL}verification/verificate`, verificationModel, { observe: 'response' }).subscribe(response => {
      this.addPost(this.globals.tempPost);
    });
  }

  public login(basicAuthenticationModel: BasicAuthenticationModel): void {
    this.credentialsCheckResponse = new CredentialsCheckResponse();
    this.httpClient.post<CredentialsCheckResponse>(`${this.apiURL}authentication/check`, basicAuthenticationModel, { observe: 'response' }).subscribe(response => {
      this.globals.user = new User();
      this.credentialsCheckResponse = response.body;
      let pin = this.smsService.send(this.credentialsCheckResponse.phone);
      this.globals.user.id = this.credentialsCheckResponse.userId;

      localStorage.setItem('user', JSON.stringify(this.globals.user));

      if (pin) {
        this.globals.user = new User();
        let verification = new Verification();
        verification.value = pin;
        verification.userId = this.credentialsCheckResponse.userId;

        this.httpClient.post<Verification>(`${this.apiURL}verification`, verification, { observe: 'response' }).subscribe(response => {
          this.router.navigate(['/verification']);
        });
      }
      else {
        this.globals.user = null;
        this.globals.error = "SMS Gateway error!"
      }
    });
  }

  public verificate(verificationModel: Verification): void {
    this.httpClient.post<User>(`${this.apiURL}verification/verificate`, verificationModel, { observe: 'response' }).subscribe(response => {
      this.user = response.body;
      this.globals.user = this.user;
      this.globals.loggedIn = true;
      localStorage.setItem('jwtResponse', JSON.stringify(this.user.jwtResponse));
      localStorage.setItem('user', JSON.stringify(this.user));
      this.router.navigate(['/' + this.user.role.name.toLowerCase() + '/dashboard']);
    });
  }

  public checkSecret(secretModel: Secret): void {
    this.httpClient.post<User>(`${this.apiURL}verification/secret`, secretModel, { observe: 'response' }).subscribe(response => {
      this.user = response.body;
      this.globals.user = this.user;
      this.globals.loggedIn = true;
      localStorage.setItem('jwtResponse', JSON.stringify(this.user.jwtResponse));
      localStorage.setItem('user', JSON.stringify(this.user));
      this.router.navigate(['/' + this.user.role.name.toLowerCase() + '/dashboard']);
    });
  }

  public googleSignIn() {
    const headers = { 'Content-Type': 'text/plain; charset=UTF-8' };
    this.httpClient.get<User>(`${this.apiURL}authentication/google`, { observe: 'response', headers: headers }).subscribe(response => {
      this.user = response.body;
      this.globals.user = this.user;
      this.globals.loggedIn = true;
      localStorage.setItem('jwtResponse', JSON.stringify(this.user.jwtResponse));
      localStorage.setItem('user', JSON.stringify(this.user));
      this.router.navigate(['/' + this.user.role.name.toLowerCase() + '/dashboard']);
    });
  }

  public logout() {
    localStorage.removeItem("jwtResponse");
    localStorage.removeItem("user");
    this.globals.user = null;
    this.globals.loggedIn = false;
    this.router.navigate(['/login']);
  }

  public isLoggedIn() {
    return new Date() < this.getExpiration();
  }

  public isLoggedOut() {
    return !this.isLoggedIn();
  }

  public deletePost(post: Post): Post[] {
    this.httpClient.post<Post[]>(`${this.apiURL}post/delete`, post, { observe: 'response' }).subscribe(response => {
      if (response.status == 200) {
        this.globals.posts = response.body;
        this.router.navigate(['/user/dashboard']);
        return response.body;
      }
      else {
        return this.globals.posts;
      }
    });

    return null;
  }

  public revertDeletePost(post: Post): Post[] {
    this.httpClient.post<Post[]>(`${this.apiURL}post/revertDeletion`, post, { observe: 'response' }).subscribe(response => {
      if (response.status == 200) {
        this.globals.posts = response.body;
        this.router.navigate(['/user/dashboard']);
        return response.body;
      }
      else {
        return this.globals.posts;
      }
    });

    return null;
  }

  public getExpiration() {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
      this.jwtResponse = this.user.jwt;
      return new Date(this.jwtResponse.expiryDate);
    }
  }
}
