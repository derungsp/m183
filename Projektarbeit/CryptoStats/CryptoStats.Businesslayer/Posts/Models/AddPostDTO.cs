﻿using System;

namespace CryptoStats.Businesslayer.Posts.Models
{
    public class AddPostDTO
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public bool Public { get; set; }

        public int UserId { get; set; }

        public DateTime Timestamp { get; set; }

    }
}
