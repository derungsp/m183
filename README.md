# M183

Pascal Derungs

## Aufgaben

Hier sind die gelösten Praxisaufgaben aufgelistet, welche im Repository vorhanden sind.

### Keylogger

Eingaben auf einer Webseite werden registriert, logisch zusammengefasst und weitergeleitet ohne dass der User etwas merkt. Die ganze Aufgbabe wurde in einer HTML Datei mittels Javascript gemacht.

### TOTP 2FA

Hier wurde in einem .NET Core Webprojekt eine Zweifaktorauthentifikation erstellt. Zum Erfassen der Authenticator App wird ein QR-Code generiert. Anschliessend muss man für das Login jeweils den Code aus der App eingeben.

In der erstellten Applikation werden vordefinierte Logindaten verwendet:
```
* Username: donald | Passwort: niceHair
* Username: joe | Passwort: kamala
```

### Clickjacking

Bei dieser Praxisaufgabe wurden Clickjacking Angriffe und mögliche Verteidigungsmassnahmen dazu angeschaut.

### UI Redressing

Hier wurde ein UI Redressing gewollt erstellt, welches ein echtes Login imitiert, jedoch die Daten weitergibt. Das ganze wurde in einer HTML Datei und mit Javascript gelöst.

```
* Diese Aufgabe wurde nicht responsive gelöst
* Zusatzaufgabe: Richtiger Login-Versuch => Daten werden 1:1 weitergegeben, ufprt Property musste hinzugefügt werden...
```

### Sichere Passwörter

Hier wurde eine einfache Webseite erstellt, die ein Input für ein Passwort besitzt und dieses dann validiert und hashed. Auch hier wurde alles in einer HTML Datei und mit Javascript gelöst. Für den Hash-Prozess und die Salt Funktion wurde "bcrypt" eingebunden.

```
* Ich habe mich für die gängigen Sicherheitsanforderung für ein Passwort entschieden. Es soll länger als 8 Zeichen lang sein, muss Gross- und Kleinbuchstaben und mindestens eine Zahl beinhalten.
* Um die Benutzer nicht zu fest zu nerven habe ich mir überlegt, eine Funktion einzubauen, bei der der Benutzer sich einfach ein Passwort generieren lassen kann, welches den Anforderungen entspricht.
* Das Passwort wird via bcrypt gehashed und dazu wird ein Salt erstellt, welche später beide persistiert werden. Das Passwort selber wird gelöscht. Ausserdem wurde für die clientseitige Validation Regex verwendet.
```

### Single Sign-On

Hier wurde eine Webapplikation erstellt, die Usern ein Login via Google erlaubt. Die Webapplikation hat eine .NET Core MVC Struktur.

```
* User können sich über Google anmelden.
* Anhand der Daten, die Google bereit stellt, wird überprüft, ob dieser Benutzer vorhanden ist oder nicht. Wenn nicht, wird ein User erstellt.
* Man kann sich ausloggen.
* Vertiefung: Ich habe privat auch schon eine Applikation mit Google Authentication erstellt, jedoch mit einem anderen Ansatz. Da ich zurzeit noch daran arbeite wollte ich nicht das ganze Projekt veröffentlichen, sondern ich habe unter dem Order "Vertiefung", die zusammenhängenden Dateien hochgeladen.
```

### HTTP Digest

Hier wurde eine simple HTTP Digest Authentication implementiert. Dazu brauchte ich HTML udn Javascript. Ausserdem wurden die Dateien .htaccess und -htpasswd erstellt.

```
* User können sich mit den vorgegebenen Anmeldedaten anmelden.
* Anmeldedaten:
* britney_s : hitMeBaby
* bon_jovi : badMedicine
* shakira : hipsDontLie
* tom_j : sexbomb
```
	
### CSRF

Hier werden 3 verschiedene CSRF-Attacken gemacht. Zie dabei ist, dass dem Benutzer Buttons gezeigt werden, die im Hintergrund eine andere Funktion verbirgt, als er sich denkt. Z.B. wird bei einem Click auf den Button, mittels Javascript, das Passwort des Benutzers geändert, ohne dass er etwas bemerkt.

### Informationsquellen

Im Ordner "10) Informationsquellen" befindet sich die readme Datei.

### Projektarbeit

Im Ordner "Projektarbeit" befindet sich ein weiteres readme.md für genauere Informationen zur Projektarbeit.