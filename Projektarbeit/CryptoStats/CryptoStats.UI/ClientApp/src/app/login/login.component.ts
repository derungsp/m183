import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BasicAuthenticationModel } from '../models/BasicAuthenticationModel';
import { Globals } from '../providers/globals';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  basicAuthenticationModel = new BasicAuthenticationModel();

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    public globals: Globals,
    private router: Router
  ) { }

  ngOnInit() {
    this.globals.error = null;
    if (this.globals.loggedIn) {
      this.router.navigate(['/']);
    }
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  googleSignIn() {
    this.apiService.googleSignIn();
  }

  onSubmit() {
    this.globals.error = null;
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.basicAuthenticationModel.username = this.loginForm.value.username;
    this.basicAuthenticationModel.password = this.loginForm.value.password;

    // Process checkout data here
    this.loginForm.reset();

    this.apiService.login(this.basicAuthenticationModel);
  }

}
