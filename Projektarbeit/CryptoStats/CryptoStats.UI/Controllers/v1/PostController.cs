﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using CryptoStats.Businesslayer.Posts.Interfaces;
using CryptoStats.Businesslayer.Posts.Models;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace CryptoStats.UI.Controllers.v1
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly ILogger<PostController> _logger;
        public PostController(IPostService postService, ILogger<PostController> logger)
        {
            _postService = postService;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<GetPostDTO>> GetAllPublicPosts()
        {
            return await _postService.GetAllPublicPosts();
        }

        [HttpGet("admin")]
        [Authorize(Roles = "Admin")]
        public async Task<IEnumerable<GetPostDTO>> GetAllPosts()
        {
            return await _postService.GetAllPosts();
        }

        [HttpGet("own")]
        public async Task<IEnumerable<GetPostDTO>> GetOwnPosts()
        {
            return await _postService.GetOwnPosts();
        }

        [HttpPost]
        public async Task<IEnumerable<GetPostDTO>> Create(AddPostDTO post)
        {
            await _postService.Create(post);

            _logger.LogInformation("New Post (" + post.Title + ") created");

            return await _postService.GetAllPublicPosts();
        }

        [HttpPut]
        public async Task<IEnumerable<GetPostDTO>> Update(UpdatePostDTO post)
        {
            await _postService.Update(post);

            _logger.LogInformation("New Post (" + post.Title + ") updated");

            return await _postService.GetAllPublicPosts();
        }

        [HttpPost("delete")]
        public Task<IEnumerable<GetPostDTO>> Delete(GetPostDTO post)
        {
            return _postService.Delete(post);
        }

        [HttpPost("revertDeletion")]
        public Task<IEnumerable<GetPostDTO>> RevertDeletion(GetPostDTO post)
        {
            return _postService.RevertDeletion(post);
        }

    }
}
