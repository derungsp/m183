﻿using CryptoStats.DataAccess.Interfaces;
using CryptoStats.DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System;

namespace CryptoStats.DataAccess.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(CryptoStatsContext dbContext) : base(dbContext)
        {
        }

        public async Task<User> GetFirstMatchingUserWithRole(Expression<Func<User, bool>> predicate)
        {
            return await _dbContext.Users.Include(x => x.Role).FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<User>> GetAllUsersWithRole()
        {
            return await _dbContext.Users.Include(x => x.Role).ToListAsync();
        }
    }
}
