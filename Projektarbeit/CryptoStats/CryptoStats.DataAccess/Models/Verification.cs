﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace CryptoStats.DataAccess.Models
{
    [Table("Verification")]
    public class Verification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int Value { get; set; }

        public DateTime Timestamp { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
